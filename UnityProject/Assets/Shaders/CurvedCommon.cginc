const static float3 k_CurvatureFactors = float3(-0.005f, 0.0f, 0.25f);	// Factors (a,b,c) in quadratic equation ax^2 + bx + c
const static float k_CurvatureOffset = 8.0f;							// Clamp curvature until this length

void curved_vs( inout appdata_full v)
{
	// Transform the vertex coordinates from model space into world space
	float4 vv = mul( _Object2World, v.vertex );

	// Now adjust the coordinates to be relative to the camera position
	vv.xyz -= _WorldSpaceCameraPos.xyz;

	// Adjust with curve offset
	vv.z = max(0, vv.z - k_CurvatureOffset);

	// Modify the y coordinate ("height") of each vertex based
	// on the square of the distance from the camera in the z axis, multiplied
	// by the chosen curvature factors
	float dy = (vv.z * vv.z * k_CurvatureFactors[0]) + (vv.z * k_CurvatureFactors[1]) + k_CurvatureFactors[2];
	vv = float4( 0.0f, dy, 0.0f, 0.0f );
	
	// Scale displacement: this will only works with uniform scaling!
	vv *= unity_Scale.w;

	// Now apply the offset back to the vertices in model space
	v.vertex += mul(_World2Object, vv);
}