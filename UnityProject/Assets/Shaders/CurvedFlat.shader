﻿Shader "Custom/Curved Flat" {

	Properties { _Color ("Color", Color) = (1,1,1) }

	SubShader {
		Tags { "RenderType" = "Opaque" }

		CGPROGRAM
		#include "CurvedCommon.cginc"
		#pragma surface surf Flat vertex:curved_vs

		half4 LightingFlat(SurfaceOutput s, half3 lightDir, half atten)
		{
			half4 c;
			c.rgb = s.Albedo;
			return c;
		}

		struct Input {
			float4 color : COLOR;
		};

		float4 _Color;

		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = _Color;
		}
		ENDCG
	}
}
