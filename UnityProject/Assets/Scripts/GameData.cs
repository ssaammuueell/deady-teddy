﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class WeaponData
{
    public string id = string.Empty;
    public string displayName = string.Empty;
    public int price = 0;
    public Sprite icon = null;
    public Weapon prefab = null;
}

public class GameData : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Name of the default input scheme.")]
    string defaultInputScheme = string.Empty;
    [SerializeField]
    [Tooltip("Number of coins the player starts with.")]
    int startingCoins = 0;
    [SerializeField]
    [Tooltip("List of weapons. The player will start with the (first) weapon with price 0.")]
    List<WeaponData> weapons = new List<WeaponData>();

    static GameData instance = null;
    public static string DefaultInputScheme { get { return instance.defaultInputScheme; } }
    public static int StartingCoins { get { return instance.startingCoins; } }
    public static List<WeaponData> Weapons { get { return instance.weapons; } }
    public static WeaponData StartingWeapon
    {
        get
        {
            // First "free" weapon
            foreach(var item in Weapons)
            {
                if (item.price <= 0)
                    return item;
            }
            return null;
        }
    }

    void Awake()
    {
        if (instance != null)
        {
            throw new Exception("GameData singleton instance already exists.");
        }
        instance = this;
    }
}
