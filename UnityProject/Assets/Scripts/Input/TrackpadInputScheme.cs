﻿using System;
using UnityEngine;

public abstract class TrackpadInputSchemeBase : IInputScheme
{
    const float DefaultPPI = 200.0f; // In case Screen.dpi = 0
    const float MaxTrackpadDisplacement = 1.0f / 2.54f; // In inches
    const bool TrackpadAdaptativeCentre = true;

    float direction;
    int trackpadFingerId;
    Rect trackpadRegion;
    Vector2 trackpadCentre;

    bool fire;
    Rect actionRegion;

    public abstract Rect NormalisedTrackpadRect { get; }
    public abstract Rect NormalisedActionRect { get; }

    public bool Fire
    {
        get { return fire; }
    }

    public float Direction
    {
        get { return direction; }
    }

    public void Update()
    {
        var touchCount = Input.touchCount;

        ResetAction();
        if (touchCount == 0)
        {
            ResetTrackpad();
        }
        else
        {
            for(int i = 0; i < touchCount; i++)
            {
                var touch = Input.touches[i];

                // Check for new trackpad finger
                if ((trackpadFingerId == -1 || trackpadFingerId != touch.fingerId) &&   // Finger not down already
                    (trackpadRegion.Contains(touch.position)))                          // Within bounds
                {
                    // Set new values
                    trackpadFingerId = touch.fingerId;
                    trackpadCentre = touch.position;
                }

                // Handle same finger
                if (trackpadFingerId == touch.fingerId)
                {
                    if(touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended)
                    {
                        ResetTrackpad();
                    }
                    else if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
                    {
                        // Get displacement in inches
                        float deltaPosX = touch.position.x - trackpadCentre.x;
                        float ppi = (Screen.dpi > float.Epsilon ? Screen.dpi : DefaultPPI);
                        float deltaInches = deltaPosX / ppi;

                        // Set direction
                        direction = Mathf.Clamp(deltaInches / MaxTrackpadDisplacement, -1, 1);

                        // Move trackpad centre
                        if (TrackpadAdaptativeCentre)
                        {
                            float deltaCentreX = Mathf.Max(0, Mathf.Abs(deltaInches) - MaxTrackpadDisplacement) * Mathf.Sign(deltaInches);
                            trackpadCentre.x += deltaCentreX;
                        }
                    }
                }

                // Check for action tap
                if ((touch.fingerId != trackpadFingerId) &&         // Not trackpad finger)
                    (actionRegion.Contains(touch.position)) &&      // Within bounds
                    (touch.phase != TouchPhase.Canceled && touch.phase != TouchPhase.Ended))
                {
                    fire = true;
                }
            }
        }
    }

    public TrackpadInputSchemeBase()
    {
        ResetTrackpad();
        ResetAction();

        // Set trackpad region
        trackpadRegion = NormalisedTrackpadRect;
        trackpadRegion.x *= Screen.width;
        trackpadRegion.y *= Screen.height;
        trackpadRegion.width *= Screen.width;
        trackpadRegion.height *= Screen.height;

        // Set action region
        actionRegion = NormalisedActionRect;
        actionRegion.x *= Screen.width;
        actionRegion.y *= Screen.height;
        actionRegion.width *= Screen.width;
        actionRegion.height *= Screen.height;
    }

    void ResetTrackpad()
    {
        direction = 0.0f;
        trackpadFingerId = -1;
        trackpadCentre = Vector2.zero;
    }

    void ResetAction()
    {
        fire = false;
    }
}

public class TrackpadInputScheme : TrackpadInputSchemeBase
{
    public override Rect NormalisedTrackpadRect
    {
        get {  return new Rect(0.0f, 0.0f, 0.5f, 0.75f); }
    }

    public override Rect NormalisedActionRect
    {
        get { return new Rect(0.5f, 0.0f, 0.5f, 0.75f); }
    }
}

public class InverseTrackpadInputScheme : TrackpadInputSchemeBase
{
    public override Rect NormalisedTrackpadRect
    {
        get { return new Rect(0.5f, 0.0f, 0.5f, 0.75f); }
    }

    public override Rect NormalisedActionRect
    {
        get { return new Rect(0.0f, 0.0f, 0.5f, 0.75f); }
    }
}