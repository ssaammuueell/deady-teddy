﻿using UnityEngine;
public class TiltInputScheme : IInputScheme
{
    const float AccelerationCap = 0.2f;
    static readonly Rect NormalisedActionRect = new Rect(0.0f, 0.0f, 1.0f, 0.75f);

    Rect actionRegion;
    float direction = 0.0f;
    bool fire = false;

    public bool Fire
    {
        get { return fire; }
    }

    public float Direction
    {
        get { return direction; }
    }

    public void Update()
    {
        // Handle direction
        float acc = Input.acceleration.x;
        acc = Mathf.Clamp(acc, -AccelerationCap, AccelerationCap);
        direction = (acc / AccelerationCap);

        // Check for firing
        fire = false;
        for(int i = 0, n = Input.touchCount; i < n; i++)
        {
            var touch = Input.touches[i];
            if (touch.phase != TouchPhase.Canceled &&
                touch.phase != TouchPhase.Ended &&
                actionRegion.Contains(touch.position))
            {
                fire = true;
                break;
            }
        }
    }

    public TiltInputScheme ()
	{
        actionRegion = NormalisedActionRect;
        actionRegion.x *= Screen.width;
        actionRegion.width *= Screen.width;
        actionRegion.y *= Screen.height;
        actionRegion.height *= Screen.height;
    }
}
