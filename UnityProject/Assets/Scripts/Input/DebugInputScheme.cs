﻿using System;
using System.Diagnostics;
using UnityEngine;

#if UNITY_STANDALONE || UNITY_EDITOR
class DebugInputScheme : IInputScheme
{
    public bool Fire
    {
        get
        {
            return // Click lower 75% of screen
                Input.GetMouseButton(0) &&
                Input.mousePosition.y < (0.75f * Screen.height);
        }
    }

    public float Direction
    {
        get { return Input.GetAxis("Horizontal"); }
    }

    public void Update()
    {
    }
}
#endif
