﻿public interface IInputScheme
{
    bool Fire { get; }
    float Direction {get; }

    void Update();
}