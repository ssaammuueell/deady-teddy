﻿using UnityEngine;

public class SettingsViewModel : ViewModel
{
    public bool MusicEnabled
    {
        get { return 0 != PlayerPrefs.GetInt("MusicEnabled", 1); }
        set
        {
            if (value != MusicEnabled)
            {
                PlayerPrefs.SetInt("MusicEnabled", value ? 1 : 0);
                OnPropertyChanged("MusicEnabled");
                EventManager.Trigger(new AudioSettingsChangedEvent());
                Debug.Log(string.Format("Music turned {0}", value ? "On" : "Off"));
            }
        }
    }
    public bool SFXEnabled
    {
        get { return 0 != PlayerPrefs.GetInt("SFXEnabled", 1); }
        set
        {
            if (value != SFXEnabled)
            {
                PlayerPrefs.SetInt("SFXEnabled", value ? 1 : 0);
                OnPropertyChanged("SFXEnabled");
                EventManager.Trigger(new AudioSettingsChangedEvent());
                Debug.Log(string.Format("Sound effects turned {0}", value ? "On" : "Off"));
            }
        }
    }

    public string InputSchemeName
    {
        get { return PlayerPrefs.GetString("InputScheme", GameData.DefaultInputScheme); }
        set
        {
            if (InputSchemeName != value)
            {
                PlayerPrefs.SetString("InputScheme", value);
                OnPropertyChanged("InputSchemeName");
                Debug.Log(string.Format("Input scheme selected: {0}", value));
            }
        }
    }

    public SettingsViewModel()
    {
    }

    public void CloseDelegate()
    {
        UIManager.ClosePopup("Settings", 0);
    }
}