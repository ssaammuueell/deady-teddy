﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ItemBag
{
    StringBuilder merged = new StringBuilder();
    List<string> items = new List<string>();
    static readonly char[] separators = {';'};
    const string prefsName = "Items";

    public ItemBag()
    {
        Load();
    }

    public void Insert(string name)
    {
        if (items.Contains(name))
            return;

        // Add item
        items.Add(name);
        merged.Append(name);
        merged.Append(separators);

        // Save
        PlayerPrefs.SetString(prefsName, merged.ToString());
    }

    public void Remove(string name)
    {
        if (!items.Contains(name))
            return;

        // Remove item
        items.Remove(name);
        merged.Replace(name + separators, string.Empty);

        // Save
        PlayerPrefs.SetString(prefsName, merged.ToString());
    }

    public bool Contains(string name)
    {
        return items.Contains(name);
    }

    void Load()
    {
        // Get concatenated values
        merged.Remove(0, merged.Length);
        merged.Append(PlayerPrefs.GetString(prefsName, string.Empty));

        // Split and get names
        string[] names = merged.ToString().Split(separators, StringSplitOptions.RemoveEmptyEntries);
        items.Clear();
        foreach(var name in names)
        {
            items.Add(name);
        }

    }
}

public class StoreItemViewModel : ViewModel
{
    private WeaponData data;
    private ItemBag bag;

    public string ID { get { return data.id; } }
    public string Name { get { return data.displayName; } }
    public int Price { get { return data.price; } }
    public Sprite Icon { get { return data.icon; } }
    public bool Owned
    {
        get { return bag.Contains(data.id); }
        set
        {
            if (value != Owned)
            {
                bag.Insert(data.id);
                OnPropertyChanged("Owned");
                OnPropertyChanged("NotOwned");
            }
        }
    }
    public bool NotOwned
    {
        get { return !Owned; }
    }

    public StoreItemViewModel(ItemBag bag, WeaponData data)
    {
        this.bag = bag;
        this.data = data;
    }
}

public class StoreViewModel : ViewModel
{
    List<StoreItemViewModel> storeItems = new List<StoreItemViewModel>();
    StoreItemViewModel storeItemSelected = null;
    ItemBag itemsOwned = new ItemBag();

    public List<StoreItemViewModel> StoreItems
    {
        get { return storeItems; }
        set
        {
            if (value != storeItems)
            {
                storeItems = value;
                OnPropertyChanged("StoreItems");
            }
        }
    }

    public StoreItemViewModel StoreItemSelected
    {
        get { return storeItemSelected; }
        set
        {
            if (value != storeItemSelected)
            {
                storeItemSelected = value;
                OnPropertyChanged("StoreItemSelected");
                SelectedItemStateChanged();
            }
        }
    }

    public int Coins
    {
        get { return PlayerPrefs.GetInt("Coins", GameData.StartingCoins); }
        set
        {
            if (value != Coins)
            {
                PlayerPrefs.SetInt("Coins", value);
                OnPropertyChanged("Coins");
            }
        }
    }

    public StoreItemViewModel StoreItemEquipped
    {
        get
        {
            string id = PlayerPrefs.GetString("EquippedItem", GameData.StartingWeapon.id);
            foreach (var it in storeItems)
                if (it.ID == id)
                    return it;
            return null;
        }
        set
        {
            if (value != StoreItemEquipped)
            {
                PlayerPrefs.SetString("EquippedItem", value == null ? GameData.StartingWeapon.id : value.ID);
                SelectedItemStateChanged();
            }
        }
    }

    public bool CanBuySelected { get { return storeItemSelected != null && !storeItemSelected.Owned && storeItemSelected.Price <= Coins; } }
    public bool CanEquipSelected { get { return storeItemSelected != null && storeItemSelected != StoreItemEquipped && storeItemSelected.Owned; } }

    public StoreViewModel()
    {
        foreach(var item in GameData.Weapons)
        {
            storeItems.Add(new StoreItemViewModel(itemsOwned, item));
        }

        // If first time opening the shop, then initialise
        // by buying starting weapon & equipping it.
        if (!PlayerPrefs.HasKey("EquippedItem"))
        {
            foreach(var vmItem in storeItems)
            {
                if (vmItem.ID == GameData.StartingWeapon.id)
                {
                    vmItem.Owned = true;
                    StoreItemEquipped = vmItem;
                    break;
                }
            }
        }

        // Select the currently equipped item
        storeItemSelected = StoreItemEquipped;
    }

    public void BuySelected()
    {
        StoreItemSelected.Owned = true;
        Coins -= StoreItemSelected.Price;
        SelectedItemStateChanged();
        Debug.Log(string.Format("Item {0} bought for {1}.", StoreItemSelected.Name, StoreItemSelected.Price));
    }

    public void EquipSelected()
    {
        StoreItemEquipped = StoreItemSelected;
        Debug.Log(string.Format("Item {0} equiped.", StoreItemSelected.Name));
    }

    void SelectedItemStateChanged()
    {
        OnPropertyChanged("CanBuySelected");
        OnPropertyChanged("CanEquipSelected");
        OnPropertyChanged("StoreItemEquipped");
    }

    public void PlayDelegate()
    {
        UIManager.SetScreen("Run");
    }

    public void BackDelegate()
    {
        UIManager.SetScreen("MainScreen");
    }
}