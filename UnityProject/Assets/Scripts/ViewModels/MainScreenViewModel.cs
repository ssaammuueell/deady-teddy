﻿using UnityEngine;

public class MainScreenViewModel : ViewModel
{
    public int BestRun { get { return PlayerPrefs.GetInt("BestRun", 0); } }

    bool settingsOpen = false;

    public void PlayDelegate()
    {
        UIManager.SetScreen("Run");
    }

    public void StoreDelegate()
    {
        UIManager.SetScreen("Store");
    }

    public void SettingsDelegate()
    {
        UIManager.OpenPopup("Settings", OnSettigsClosed);
        settingsOpen = true;
    }

    void OnSettigsClosed(object obj)
    {
        settingsOpen = false;
    }

    public void BackDelegate()
    {
        if (settingsOpen)
            UIManager.ClosePopup("Settings", null);
        else
            Application.Quit();
    }
}
