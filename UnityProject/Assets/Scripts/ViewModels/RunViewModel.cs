﻿using UnityEngine;

public struct RunStateChangedEvent
{
    public enum State
    {
        Active,     // Currently playing
        Paused,     // Game is paused
        Resuming,   // Resuming countdown
        Canceled,   // Player quit without finishing
        Ended       // Run over
    }

    public State state;
}

public class RunQuitViewModel : ViewModel
{
    public void QuitDelegate()
    {
        EventManager.Trigger(new RunStateChangedEvent() { state = RunStateChangedEvent.State.Canceled });
    }
}

public class RunViewModel : ViewModel
{
    int distance = 0;
    int kills = 0;
    int ammo = 0;
    bool isLoading = true;

    public int Distance
    {
        get { return distance; }
        set
        {
            if (value != distance)
            {
                distance = value;
                OnPropertyChanged("Distance");
            }
        }
    }
    public int Kills
    {
        get { return kills; }
        set
        {
            if (value != kills)
            {
                kills = value;
                OnPropertyChanged("Kills");
            }
        }
    }
    public int Ammo
    {
        get { return ammo; }
        set
        {
            if (value != ammo)
            {
                ammo = value;
                OnPropertyChanged("Ammo");
            }
        }
    }

    public bool IsLoading
    {
        get { return isLoading; }
        private set
        {
            if (isLoading != value)
            {
                isLoading = value;
                OnPropertyChanged("IsLoading");
            }
        }
    }

    bool isQuitting = false;
    RunStateChangedEvent.State state = RunStateChangedEvent.State.Active;

    public RunViewModel()
    {
        EventManager.AddListener<RunStateChangedEvent>(OnRunStateChanged);
        EventManager.AddListener<PlayerDistanceChangedEvent>(OnDistanceChanged);
        EventManager.AddListener<AmmoChangedEvent>(OnAmmoChanged);
        EventManager.AddListener<EnemyDeathEvent>(OnKillsChanged);
        EventManager.AddListener<LevelLoadedEvent>(OnLevelLoaded);

        Application.LoadLevelAdditiveAsync("Run");
    }

    public override void Dispose()
    {
        base.Dispose();
        EventManager.RemoveListener<RunStateChangedEvent>(OnRunStateChanged);
        EventManager.RemoveListener<PlayerDistanceChangedEvent>(OnDistanceChanged);
        EventManager.RemoveListener<AmmoChangedEvent>(OnAmmoChanged);
        EventManager.RemoveListener<EnemyDeathEvent>(OnKillsChanged);
        EventManager.RemoveListener<LevelLoadedEvent>(OnLevelLoaded);
    }

    private void OnLevelLoaded(LevelLoadedEvent e)
    {
        System.GC.Collect(); // Collect before starting gameplay
        IsLoading = false;
    }

    private void OnKillsChanged(EnemyDeathEvent e)
    {
        Kills++;
    }

    private void OnAmmoChanged(AmmoChangedEvent e)
    {
        Ammo = e.ammo;
    }

    private void OnDistanceChanged(PlayerDistanceChangedEvent e)
    {
        Distance = (int)e.distance;
    }

    public void SettingsDelegate()
    {
        EventManager.Trigger(new RunStateChangedEvent() { state = RunStateChangedEvent.State.Paused });
        UIManager.OpenPopup("Settings", OnSettingsClosed);
        UIManager.OpenPopup("RunQuit", null);

        // Stop
        Time.timeScale = 0.0f;
    }

    public void BackDelegate()
    {
        switch(state)
        {
                // If active, back button opens open the settings menu
            case RunStateChangedEvent.State.Active:
                SettingsDelegate();
                break;
                // If the settings menu is open, it closes it
            case RunStateChangedEvent.State.Paused:
                UIManager.ClosePopup("Settings", null);
                break;

            default: // Nothing to do
                break;
        }
    }

    void OnSettingsClosed(object val)
    {
        // Settings window might be closed due to Quit being clicked
        // or the user going back to the game by closing the settings popup
        if (!isQuitting)
        {
            // Not quitting, then remove the quit popup too
            UIManager.ClosePopup("RunQuit", null);
            UIManager.OpenPopup("ResumeCountdown", Resume);
            EventManager.Trigger(new RunStateChangedEvent() { state = RunStateChangedEvent.State.Resuming });
        }
        else
        {
            Time.timeScale = 1.0f; // Make sure time scale is restored regardless
        }
    }

    void Resume(object val)
    {
        // Resume
        Time.timeScale = 1.0f;
        EventManager.Trigger(new RunStateChangedEvent() { state = RunStateChangedEvent.State.Active });
    }

    void OnRunStateChanged(RunStateChangedEvent e)
    {
        this.state = e.state;

        switch(e.state)
        {
            case RunStateChangedEvent.State.Ended:
                OnRunOver();
                break;
            case RunStateChangedEvent.State.Canceled:
                OnRunQuit();
                break;


            default:
                break;
        }
    }

    void OnRunQuit()
    {
        isQuitting = true;
        UIManager.SetScreen("MainScreen");
    }

    void OnRunOver()
    {
        var popup = UIManager.OpenPopup("GameOver", null);
        var binder = popup.AddComponent<ChildViewModelBinder>();
        binder.SetViewModel(new GameOverViewModel(Distance, Kills));
    }
}