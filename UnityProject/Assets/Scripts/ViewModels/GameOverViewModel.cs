﻿using UnityEngine;
using System.Collections;

public class GameOverViewModel : ViewModel
{
    int kills;
    int distance;
    bool newBestRun;
    bool runFaded = false;

    public int Kills { get { return kills; } }
    public int Distance { get { return distance; } }
    public int BestRun{ get { return PlayerPrefs.GetInt("BestRun", 0); } }
    public bool NewBestRun { get { return newBestRun; } }
    public int Reward { get { return CalculateReward(); } }
    public bool RunFaded
    {
        get { return runFaded; }
        set
        {
            if (runFaded != value)
            {
                runFaded = value;
                OnPropertyChanged("RunFaded");
            }
        }
    }

    public GameOverViewModel(int distance, int kills)
    {
        this.distance = distance;
        this.kills = kills;

        newBestRun = (distance > BestRun);
        if (newBestRun)
            PlayerPrefs.SetInt("BestRun", distance);

        int coins = PlayerPrefs.GetInt("Coins", GameData.StartingCoins);
        PlayerPrefs.SetInt("Coins", coins + Reward);
        EventManager.AddListener<GameOverFadeCompleted>(OnFadeCompleted);
    }

    int CalculateReward()
    {
        return
            (Distance / 20) +                   // Distance reward
            (1 * Kills) +                       // Kill reward
            (NewBestRun ? (Distance / 15) : 0); // New best bonus
    }

    private void OnFadeCompleted(GameOverFadeCompleted e)
    {
        EventManager.RemoveListener<GameOverFadeCompleted>(OnFadeCompleted);
        RunFaded = true;
    }

    public void BackDelegate()
    {
        UIManager.SetScreen("MainScreen");
    }

    public void AgainDelegate()
    {
        UIManager.SetScreen("Run");
    }
}