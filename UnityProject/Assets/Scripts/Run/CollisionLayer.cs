﻿using UnityEngine;
using System.Collections;

public enum CollisionLayer : int
{
    Player = 8,
    PlayerTrigger,
    Enemy,
    EnemtTrigger,
    Obstacle,
    ObstacleTrigger,
    EnemyView,
    Pickup,
    PickupTrigger,
    Floor,
    ViewHorizon
}

public static class CollisionLayerExt
{
    public static int ToInt(this CollisionLayer layer)
    {
        return (int)layer;
    }
    public static int Mask(this CollisionLayer layer)
    {
        return (1 << (int)layer);
    }
}

public static class CollisionMask
{
    public static int Get(params CollisionLayer[] layers)
    {
        int mask = 0;
        foreach (var l in layers)
            mask |= (1 << (int)l);
        return mask;
    }
}