﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
public struct SetInvincibleEvent
{
    public bool value;
}
#endif

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PlayerController))]
public class PlayerHealth : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Sound to make when killed.")]
    AudioClip deathSound = null;

    bool isDead = false;

#if UNITY_EDITOR
    void Awake()
    {
        EventManager.AddListener<SetInvincibleEvent>(SetInvincible);
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<SetInvincibleEvent>(SetInvincible);
    }

    bool isInvincible = false;
    void SetInvincible(SetInvincibleEvent e)
    {
        isInvincible = e.value;
    }

#endif

    void OnCollisionEnter(Collision collision)
    {
        Collider collider = collision.collider;
        // Filter out non enemies
        if (collider.gameObject.layer != CollisionLayer.Enemy.ToInt())
            return;

#if UNITY_EDITOR
        if (isInvincible)
            return;
#endif


        // Let the killer know & stop him
        collider.GetComponent<Animator>().SetTrigger("PlayerDead");
        collider.GetComponent<EnemyController>().enabled = false;

        if (isDead)
            return;

        // Disable controller & start death sequence
        GetComponent<PlayerController>().enabled = false;
        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<Animator>().SetTrigger("Die");
        isDead = true;

        AudioManager.BackgroundMusic = null;
        AudioManager.PlaySoundEffect(deathSound);

        EventManager.Trigger(new RunStateChangedEvent() { state = RunStateChangedEvent.State.Ended });
    }

    void DeathAnimationEnded() // AnimationEvent
    {
    }
}
