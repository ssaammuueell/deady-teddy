﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct PlayerDistanceChangedEvent
{
    public float distance;
}

[RequireComponent(typeof(WeaponController))]
[RequireComponent(typeof(CapsuleCollider))]
public class PlayerController : MonoBehaviour
{
#if UNITY_STANDALONE || UNITY_EDITOR
    static readonly IInputScheme debugInputScheme = new DebugInputScheme();
#endif
    static readonly IInputScheme[] inputSchemes =
    {
        new TrackpadInputScheme(),
        new InverseTrackpadInputScheme(),
        new TiltInputScheme()
    };

    [SerializeField]
    [Tooltip("Forward speed.")]
    float forwardSpeed = 0.0f;
    [SerializeField]
    [Tooltip("Sideways speed.")]
    float horizontalSpeed = 0.0f;
    [SerializeField]
    [Tooltip("Speed during obstacle avoidance.")]
    float obstacleAvoidanceSpeed = 0.0f;
    [SerializeField]
    [Tooltip("Turning speed during obstacle avoidance.")]
    float obstacleAvoidanceTurningSpeed = 0.0f;
    [SerializeField]
    [Tooltip("Margin to keep from colliding geometry (obstacles & walls)")]
    float collisionMargin = 0.2f;

    IInputScheme activeInputScheme = null;
    WeaponController weaponController;
    bool isPaused = false;

    ObstacleColliderInfo avoidingObstacle = null;
    float avoidingTargetX = 0.0f;

    void Awake()
    {
        EventManager.AddListener<RunStateChangedEvent>(OnRunStateChanged);

        weaponController = GetComponent<WeaponController>();
    }

    void Start()
    {
        UpdateInputScheme();
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<RunStateChangedEvent>(OnRunStateChanged);
    }

	void Update ()
    {
        if (isPaused || activeInputScheme == null)
            return;

        // Update active input scheme & retrive results
        activeInputScheme.Update();
        var dir = activeInputScheme.Direction;
        var fire = activeInputScheme.Fire;

        UpdateMovement(dir);
        EventManager.Trigger(new PlayerDistanceChangedEvent() { distance = transform.position.z });

        // Let the weapon controller handle the weapon logic
        weaponController.UpdateWeaponController(fire);
	}

    private void UpdateMovement(float dir)
    {
        // Free-moving
        if (avoidingObstacle == null)
        {
            float dforward = forwardSpeed * Time.deltaTime;
            float dside = horizontalSpeed * dir * Time.deltaTime;

            // Move player
            transform.Translate((dforward * transform.forward) + (dside * transform.right));

            // Turn to forward
            Quaternion forwardRot = Quaternion.LookRotation(Vector3.forward);
            float t = Mathf.Clamp01(Time.deltaTime * Mathf.Deg2Rad * obstacleAvoidanceTurningSpeed);
            transform.rotation = Quaternion.Slerp(transform.rotation, forwardRot, t);

            return;
        }

        var position = transform.position;

        // We are avoiding an obstacle
        float dist = avoidingTargetX - position.x;
        float dx = obstacleAvoidanceSpeed * Mathf.Sign(dist) * Time.deltaTime;
        transform.Translate(new Vector3(dx, 0.0f, 0.0f), Space.World);

        // Face left or right + a bit forward
        var lookDir = (0.5f*Vector3.forward + Mathf.Sign(dist) * Vector3.right).normalized;
        Quaternion rotation = Quaternion.LookRotation(lookDir);
        float tt = Time.deltaTime * Mathf.Deg2Rad * obstacleAvoidanceTurningSpeed;
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, tt);

        // Have we moved past the target? if so stop avoiding
        if (Mathf.Abs(dx) >= Mathf.Abs(dist))
        {
            avoidingObstacle = null;
        }
    }

    void OnRunStateChanged(RunStateChangedEvent e)
    {
        switch (e.state)
        {
            case RunStateChangedEvent.State.Paused:
                isPaused = true;
                break;
            case RunStateChangedEvent.State.Active:
                isPaused = false;
                UpdateInputScheme(); // Input scheme may have changed
                break;


            default:
                break;
        }
    }

    void UpdateInputScheme()
    {
        string name = PlayerPrefs.GetString("InputScheme", GameData.DefaultInputScheme);

        // Set new input scheme
        foreach (var scheme in inputSchemes)
        {
            if (scheme.GetType().Name == name)
            {
                activeInputScheme = scheme;
                break;
            }
        }

#if UNITY_EDITOR
        activeInputScheme = debugInputScheme;
#endif
    }

    void OnCollisionStay(Collision collision)
    {
        if (avoidingObstacle == null)
            OnCollisionEnter(collision);
    }
    void OnCollisionEnter(Collision collision)
    {
        Collider collider = collision.collider;

        // Only interested in obstacles
        if (collider.gameObject.layer != CollisionLayer.Obstacle.ToInt())
            return;

        var capsule = GetComponent<CapsuleCollider>();

        // Check if colliding from front, if not, then no need to walk around object
        Vector3 castStart = transform.position + capsule.center;
        RaycastHit hit;
        if (!Physics.SphereCast(castStart, capsule.radius, transform.forward, out hit, 2.0f * capsule.radius, CollisionLayer.Obstacle.Mask()))
            return;


        // Get obstacle info
        var info = collider.GetComponent<ObstacleColliderInfo>();
        avoidingObstacle = info;

        // There is an obstacle within lookeahead distance so
        // we move towards the side which is going to let us avoid the
        // obstacle faster
        float lengthLeft = transform.position.x - info.Bounds.min.x;
        float lengthRight = info.Bounds.max.x - transform.position.x;
        float minGap = (capsule.radius + collisionMargin) * 2.0f; // diamater + wall margin + obstacle margin
        bool left = (lengthLeft < lengthRight || avoidingObstacle.RightWallGap < minGap);

        // Set obstacle to avoid & which side to do so from, we try to
        // move towards the side the player is currently moving to unless
        // we dont fit through the gap between the obstacle & the wall
        if (left)
            avoidingTargetX = avoidingObstacle.Bounds.min.x - capsule.radius - collisionMargin;
        else
            avoidingTargetX = avoidingObstacle.Bounds.max.x + capsule.radius + collisionMargin;
    }

}
