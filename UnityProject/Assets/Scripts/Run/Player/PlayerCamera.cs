﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Player transform")]
    Transform playerTransform = null;

    Vector3 offset;

    void Awake()
    {
        if (playerTransform == null)
            throw new MissingReferenceException("PlayerCamera is missing a reference to the player transform.");

        // Get offset from scene initial values
        offset = transform.position - playerTransform.position;
    }

    void Update()
    {
        transform.position = playerTransform.position + offset;
    }
}
