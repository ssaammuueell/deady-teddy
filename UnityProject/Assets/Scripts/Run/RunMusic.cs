﻿using UnityEngine;
using System.Collections;

public class RunMusic : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Volume while playing game.")]
    [Range(0.0f, 1.0f)]
    float playingVolume = 1.0f;
    [SerializeField]
    [Tooltip("Volume while game paused.")]
    [Range(0.0f, 1.0f)]
    float pausedVolume = 0.0f;
    [SerializeField]
    [Tooltip("Fade in & out speed (unit/second).")]
    float fadeInOutSpeed = 1.0f;
    [SerializeField]
    [Tooltip("Backgound music song.")]
    AudioClip song;

    float targetVolume;

    void Awake()
    {
        EventManager.AddListener<RunStateChangedEvent>(OnRunStateChanged);
    }

    void Start()
    {
        AudioManager.BackgroundMusic = song;
        AudioManager.MusicVolume = 0.0f;
        targetVolume = playingVolume;
    }

    void OnDisable()
    {
    }

    void OnDestroy()
    {
        AudioManager.BackgroundMusic = null;
        EventManager.RemoveListener<RunStateChangedEvent>(OnRunStateChanged);
    }

    void OnRunStateChanged(RunStateChangedEvent e)
    {
        switch (e.state)
        {
            case RunStateChangedEvent.State.Paused:
                targetVolume = pausedVolume;
                break;
            case RunStateChangedEvent.State.Resuming:
                targetVolume = playingVolume;
                break;
        }
    }

    void Update()
    {
        // Update volume if it isnt at the target volume to
        // achieve fadein/fadeout effects. Time is paused when
        // the run is paused hence unscaled time.
        float delta = targetVolume - AudioManager.MusicVolume;
        if (Mathf.Abs(delta) > float.Epsilon)
        {
            AudioManager.MusicVolume = (delta > 0.0f)
                ? Mathf.Min(AudioManager.MusicVolume + Time.unscaledDeltaTime * fadeInOutSpeed, targetVolume)
                : Mathf.Max(AudioManager.MusicVolume - Time.unscaledDeltaTime * fadeInOutSpeed, targetVolume);
        }
    }
}
