﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
public struct FillAmmoEvent { }
#endif

public class WeaponController : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Gun attachment joint")]
    Transform gunAttachPoint = null;


    List<Weapon> weapons = new List<Weapon>();

    private Weapon activeWeapon = null;
    void Awake()
    {
        if (gunAttachPoint == null)
            throw new MissingReferenceException("WeaponController gun attachment point not set.");
        EventManager.AddListener<PickupEvent>(OnPickupEvent);
#if UNITY_EDITOR
        EventManager.AddListener<FillAmmoEvent>(OnFillAmmo);
#endif
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<PickupEvent>(OnPickupEvent);
#if UNITY_EDITOR
        EventManager.RemoveListener<FillAmmoEvent>(OnFillAmmo);
#endif
    }
#if UNITY_EDITOR
    void OnFillAmmo(FillAmmoEvent e)
    {
        if (activeWeapon != null)
            activeWeapon.FillAmmo();
    }
#endif

	void Start()
    {
        // Load weapons and set starting one
        LoadWeapons();
        SetWeapon(PlayerPrefs.GetString("EquippedItem", GameData.StartingWeapon.id));
        if (activeWeapon != null)
            activeWeapon.FillAmmo();
    }

	public void UpdateWeaponController(bool fireDown)
    {
        if (activeWeapon != null)
            activeWeapon.UpdateWeapon(fireDown);
	}

    void LoadWeapons()
    {
        // Load & instantiate the weapons
        for (int i = 0, n = GameData.Weapons.Count; i < n; i++)
        {
            var data = GameData.Weapons[i];
            var instance = GameObject.Instantiate(data.prefab) as Weapon;
            instance.transform.SetParent(gunAttachPoint, false);
            instance.name = data.id;
            instance.gameObject.SetActive(false);

            // Store
            weapons.Add(instance.GetComponent<Weapon>());
        }
    }

    void SetWeapon(string id)
    {
        Weapon newWeapon = null;

        // Find weapon by id
        foreach (var weapon in weapons)
        {
            if (weapon.name.Equals(id))
            {
                newWeapon = weapon;
                break;
            }
        }

        // Same weapon?
        if (newWeapon == activeWeapon)
            return;

        // Ensure only one active
        if (activeWeapon != null)
            activeWeapon.gameObject.SetActive(false);

        // Update weapon
        activeWeapon = newWeapon;
        if (activeWeapon != null)
            activeWeapon.gameObject.SetActive(true);
    }

    void OnPickupEvent(PickupEvent e)
    {
        if (!string.IsNullOrEmpty(e.pickup.WeaponID))
            SetWeapon(e.pickup.WeaponID);

        if (activeWeapon != null)
            activeWeapon.FillAmmo();

        AudioManager.PlaySoundEffect(e.pickup.Sound);
    }
}
