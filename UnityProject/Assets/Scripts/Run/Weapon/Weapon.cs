﻿using UnityEngine;
using System.Collections.Generic;


public struct AmmoChangedEvent
{
    public int ammo;
}

public class Weapon : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Time between shots in seconds.")]
    float firePeriod = 1.0f;
    [SerializeField]
    [Tooltip("Maximum capacity.")]
    int ammoCapacity = 1;
    [SerializeField]
    [Tooltip("Maximum range of bullets.")]
    float range = 10.0f;
    [SerializeField]
    [Tooltip("Radius of bullets for collision testing.")]
    float bulletRadius = 0.05f;
    [SerializeField]
    [Tooltip("Is this an automatic weapon (continuous stream of bullets)?")]
    bool automatic = false;
    [SerializeField]
    [Tooltip("Shooting audio clip.")]
    AudioClip audioClip = null;


    BulletSourceTag bulletSource;
    float timeCountdown = 0.0f;
    int ammoRemaining;

    bool lastFireDown = false;

    void Awake()
    {
        bulletSource = FindObjectOfType<BulletSourceTag>();
        if (bulletSource == null)
            throw new System.NullReferenceException("Could not find bullet source in scene.");
    }

    public void FillAmmo()
    {
        ammoRemaining = ammoCapacity;
        EventManager.Trigger(new AmmoChangedEvent() { ammo = ammoRemaining });
    }

    public void UpdateWeapon(bool fireDown)
    {
        // Handle tapping
        bool tap = (fireDown && !lastFireDown);
        lastFireDown = fireDown;

        // Update time until we can shoot again
        float timeRemaining = timeCountdown - Time.deltaTime;
        timeCountdown = Mathf.Max(0.0f, timeRemaining);
        if (timeRemaining > 0.0f)
            return;
        
        // If we can't shoot, we are done
        if (!fireDown ||
            timeRemaining > 0.0f ||
            ammoRemaining <= 0 ||
            (!tap && !automatic))
            return;


        // Perform shot
        Shoot();
        AudioManager.PlaySoundEffect(audioClip);

        // Update stats
        timeCountdown = timeRemaining + firePeriod;
        ammoRemaining--;
        EventManager.Trigger(new AmmoChangedEvent() { ammo = ammoRemaining });
    }

    void Shoot()
    {
        Vector3 start = bulletSource.transform.position;

        // Random direction using weapon parameters
        Vector3 dir = bulletSource.transform.forward;

        // Shoot bullet
        ShootBullet(start, dir);
        Debug.DrawLine(start, start + (range * dir), Color.red, 0.5f);
    }

    void ShootBullet(Vector3 start, Vector3 dir)
    {
        RaycastHit hitInfo;
        if (Physics.SphereCast(start, bulletRadius, dir, out hitInfo, range, CollisionLayer.Enemy.Mask()))
        {
            GameObject target = hitInfo.collider.gameObject;
            var enemyHealth = target.GetComponent<EnemyHealth>();
            if (enemyHealth != null)
            {
                enemyHealth.ReceiveHit(hitInfo.point);
            }
        }
    }
}
