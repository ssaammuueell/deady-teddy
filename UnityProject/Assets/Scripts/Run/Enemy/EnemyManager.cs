﻿using System.Collections.Generic;
using UnityEngine;

public struct RemoveEnemyRequest
{
    public GameObject enemy;
}

public class EnemyManager : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Enemy prefab to use.")]
    GameObject enemyPrefab = null;

    public GameObject EnemyPrefab { get { return enemyPrefab; } }

    Queue<GameObject> recycledEnemies = new Queue<GameObject>();

	void Awake()
    {
        EventManager.AddListener<RemoveEnemyRequest>(OnEnemyRemoveRequest);
        if (enemyPrefab == null)
            throw new MissingReferenceException("EnemyManager missing prefab property value.");
	}

    public GameObject SpawnEnemy()
    {
        GameObject enemy;
        if (recycledEnemies.Count > 0)
        {
            // Get recycled enemy and set transform to original
            enemy = recycledEnemies.Dequeue();
            enemy.transform.localPosition = enemyPrefab.transform.localPosition;
            enemy.transform.localRotation = enemyPrefab.transform.localRotation;
            enemy.transform.localScale = enemyPrefab.transform.localScale;
            enemy.SetActive(true);

            // Enemy components need to implement ResetEnemy() to reset state data
            enemy.SendMessage("ResetEnemy");
        }
        else
        {
            enemy = (GameObject)GameObject.Instantiate(enemyPrefab);
            enemy.name = enemyPrefab.name;
            enemy.transform.SetParent(this.transform);

            // For some strange reason on wp8, unless you do this
            // the OnCollisionEnter() event isn't being called.
            // Is this a unity bug?
            // TODO SP - investigate
            enemy.SetActive(false);
            enemy.SetActive(true);
        }


        Debug.Log("Enemy spawned.");
        return enemy;
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<RemoveEnemyRequest>(OnEnemyRemoveRequest);
    }


    private void OnEnemyRemoveRequest(RemoveEnemyRequest e)
    {
        // If this manager didnt spawn it, ignore
        if (!e.enemy.name.Equals(enemyPrefab.name))
            return;

        RecycleEnemy(e.enemy);
        Debug.Log("Enemy removed.");
    }

    private void RecycleEnemy(GameObject enemy)
    {
        enemy.SetActive(false);
        recycledEnemies.Enqueue(enemy);
    }
	
}
