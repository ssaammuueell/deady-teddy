﻿using UnityEngine;
using System.Collections;

public struct EnemyDeathEvent
{
    public GameObject enemy;
}

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CapsuleCollider))]
public class EnemyHealth : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Sound effect to play when killed.")]
    AudioClip deathNoise = null;
    [SerializeField]
    [Tooltip("Sinking speed when the enemy is killed.")]
    float sinkSpeed = 0.0f;
    [SerializeField]
    [Tooltip("Distance to sink before we can get rid of the enemy. Make sure it still intersects with the EnemyLeftBehind collider.")]
    float sinkDistance = 0.0f;

    ParticleSystem particles;

    void Awake()
    {
        particles = GetComponentInChildren<ParticleSystem>();
    }

    void Start()
    {
        ResetEnemy();
    }

    public void ReceiveHit(Vector3 hitPoint)
    {
        // Play shot particle effect where the enemy was hit
        particles.transform.position = hitPoint;
        particles.Play();

        // Set collider to trigger to avoid further shots
        GetComponent<CapsuleCollider>().isTrigger = true;

        AudioManager.PlaySoundEffect(deathNoise);
        GetComponent<Animator>().SetTrigger("Dead");

        // Notify enemy's death
        EventManager.Trigger(new EnemyDeathEvent() { enemy = gameObject });
    }

    void ResetEnemy()
    {
        GetComponent<CapsuleCollider>().isTrigger = false;
    }

    void StartSinking() // AnimationEvent
    {
        StartCoroutine(SinkEnemy(this));
    }

    static IEnumerator SinkEnemy(EnemyHealth enemy)
    {
        // Sink down the enemy until it reaches the defined sink distance
        float yTarget = enemy.transform.localPosition.y - enemy.sinkDistance;
        while (true)
        {
            enemy.transform.Translate(-Vector3.up * enemy.sinkSpeed * Time.deltaTime);
            if (enemy.transform.localPosition.y <= yTarget)
                break;
            yield return null;
        }
    }
}
