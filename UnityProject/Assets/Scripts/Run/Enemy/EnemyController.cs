﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CapsuleCollider))]
public class EnemyController : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Forward speed.")]
    float speed = 2.0f;
    [SerializeField]
    [Tooltip("Turning speed (degrees per second).")]
    float turningSpeed = 45.0f;
    [SerializeField]
    [Tooltip("Lookahead distance for obstacle avoidance.")]
    float obstacleLookahead = 10.0f;
    [SerializeField]
    [Tooltip("Margin offset to keep from obstacles.")]
    float obstacleMargin = 0.3f;

    float castRadius;
    float minObstacleGap;
    Vector3 rayOffset;

    public Vector3 TargetPosition { get; set; }

    void Awake()
    {
        var capsule = GetComponent<CapsuleCollider>();
        castRadius = capsule.radius + obstacleMargin;
        minObstacleGap = 2 * capsule.radius + obstacleMargin;
        rayOffset = capsule.center;
    }

    void Start()
    {
        ResetEnemy();
    }

	void Update()
    {
        Vector3 position = transform.position;

        RaycastHit hitInfo;
        if (Physics.SphereCast(position + rayOffset, castRadius, transform.forward, out hitInfo, obstacleLookahead, CollisionLayer.Obstacle.Mask()))
        {
            var obstacle = hitInfo.collider.GetComponent<ObstacleColliderInfo>();

            // There is an obstacle within lookeahead distance so
            // we move towards the side of the shortest path that we
            // fit through
            float lengthLeft = position.x - obstacle.Bounds.min.x;
            float lengthRight = obstacle.Bounds.max.x - position.x;
            bool moveTowardsLeftWall =
                (obstacle.LeftWallGap >= minObstacleGap) &&
                ((lengthLeft < lengthRight) || (obstacle.RightWallGap < minObstacleGap));

            // Note: for an enemy to move towards the right wall it needs to move left
            // from its point of view and vice-versa
            float angularVelocity = (moveTowardsLeftWall ? turningSpeed : -turningSpeed);
            transform.Rotate(transform.up, angularVelocity * Time.deltaTime);
        }
        else
        {
            // If we are not moving towards the target rotate towards it
            Vector3 targetDir = (TargetPosition - position).normalized;
            Quaternion rotation = Quaternion.LookRotation(targetDir);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * Mathf.Deg2Rad * turningSpeed);
        }

        // Move forward
        transform.position += (transform.forward * speed * Time.deltaTime);        
	}

    void ResetEnemy()
    {
        TargetPosition = new Vector3(transform.position.x, transform.position.y, 0.0f);
    }
}
