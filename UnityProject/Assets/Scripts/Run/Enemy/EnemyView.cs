﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyView : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Set the target position this distance ahead of it.")]
    float aheadDistanceTarget;

    int triggerCount = 0;
    Transform player;

    EnemyController controller;

    void Awake()
    {
        controller = transform.parent.GetComponent<EnemyController>();
        if (controller == null)
            throw new MissingReferenceException("EnemyView could not find the enemy controller.");
    }

    void OnTriggerEnter(Collider collider)
    {
        triggerCount++;
        player = collider.transform;
    }


    void Update()
    {
        if (triggerCount < 1)
            return;


        // Target the player
        float dz = controller.transform.position.z - player.position.z;
        float ahead = Mathf.Clamp(dz, 0.0f, aheadDistanceTarget);
        controller.TargetPosition= new Vector3(
            player.position.x,
            controller.transform.position.y,
            controller.transform.position.z - ahead
            );
    }

    void OnTriggerExit(Collider collider)
    {
        triggerCount--;
    }
}