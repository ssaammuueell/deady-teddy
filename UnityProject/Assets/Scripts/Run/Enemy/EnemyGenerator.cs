﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyGenerator : MonoBehaviour
{
    [SerializeField]
    [Tooltip("How often to change the enemy distribution function.")]
    float distributionChangePeriod = 1.0f;
    [SerializeField]
    [Tooltip("Multiplier for the spawn rate function.")]
    float spawnRateFactor = 1.0f;
    [SerializeField]
    float logFunctionOffset = 0.0f;
    [SerializeField]
    float logFunctionFactor = 0.01f;
    [SerializeField]
    float linearFactor = 1.0f;

    EnemyManager[] enemyManagers;
    bool[] enemyManagersMask;

    WorldChunkActiveEvent? lastActiveEvent = null;
    WorldChunkInViewEvent? lastInViewEvent = null;

    delegate float DistributionFunction(float hrangex);

    DistributionFunction[] distributions = new DistributionFunction[]
    {
        SinDistribution,
        RandomDistribution,
        //CosDistribution
    };

    int currentDistribution = 0;
    float queuedEnemies = 0.0f;

    void Awake()
    {
        // Get enemy managers
        enemyManagers = this.GetComponents<EnemyManager>();
        enemyManagersMask = new bool[enemyManagers.Length];

        EventManager.AddListener<WorldChunkActiveEvent>(OnChunkActive);
        EventManager.AddListener<WorldChunkInViewEvent>(OnChunkInView);
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<WorldChunkActiveEvent>(OnChunkActive);
        EventManager.RemoveListener<WorldChunkInViewEvent>(OnChunkInView);
    }

	void Start()
    {
        StartCoroutine(DistributionUpdater(this));
        StartCoroutine(SpawnQueueUpdater(this));
    }

    void Update()
    {
        while (queuedEnemies >= 1.0f)
        {
            queuedEnemies -= 1.0f;
            SpawnEnemy();
        }
    }

    float Distance { get { return lastInViewEvent == null ? 0.0f : lastInViewEvent.Value.playerTransform.position.z; } }
    float Horizon { get { return lastInViewEvent == null ? 0.0f : lastInViewEvent.Value.viewHorizon.position.z; } }

    void OnChunkInView(WorldChunkInViewEvent e)
    {
        lastInViewEvent = e;
        UpdateMask(e.worldRegion.EnemiesAllowed);
    }

    void OnChunkActive(WorldChunkActiveEvent e)
    {
        lastActiveEvent = e;
    }

    void SpawnEnemy()
    {
        if (enemyManagers.Length > 0)
        {
            int i;
            do
            {
                i = Random.Range(0, enemyManagers.Length);
            }
            while (enemyManagersMask[i] == false);
            var newEnemy = enemyManagers[i].SpawnEnemy();


            // Calculate the range of possible position offsets
            Vector3 enemyBounds = GetEnemyBounds(newEnemy);
            float hrangex = 0.5f * (lastInViewEvent.Value.distanceBetweenWalls - enemyBounds.x);

            // Put the enemy in a random position within the range above
            // plus the offset to the spawn box
            var dist = distributions[currentDistribution];
            Vector3 offset = new Vector3(dist(hrangex), 0.0f, Horizon);
            newEnemy.transform.position += offset;
        }
    }

    static float SinDistribution(float hrangex)
    {
        float r = Random.Range(-0.5f * Mathf.PI, 0.5f * Mathf.PI);
        float s = Mathf.Sin(r);

        return s * hrangex;
    }
    static float CosDistribution(float hrangex)
    {
        float r = Random.Range(-0.5f * Mathf.PI, 0.5f * Mathf.PI);
        float s = Mathf.Cos(r) * Mathf.Sin(r);

        return s * hrangex;
    }
    static float RandomDistribution(float hrangex)
    {
        return Random.Range(-1.0f, 1.0f) * hrangex;
    }

    static IEnumerator DistributionUpdater(EnemyGenerator g)
    {
        var waitTime = new WaitForSeconds(g.distributionChangePeriod);
        while (true)
        {
            g.currentDistribution = Random.Range(0, g.distributions.Length);
            yield return waitTime;
        }
    }

    static IEnumerator SpawnQueueUpdater(EnemyGenerator g)
    {
        while (true)
        {
            if (g.lastInViewEvent != null)
            {
                float intensity = Mathf.Log(1.0f + g.logFunctionOffset + (g.Distance * g.logFunctionFactor))
                    * g.lastInViewEvent.Value.worldRegion.EnemyIntensity
                    * g.linearFactor;
                g.queuedEnemies += intensity * g.spawnRateFactor * Time.deltaTime;
            }
            yield return null;
        }
    }

    Vector3 GetEnemyBounds(GameObject newEnemy)
    {
        // Use the collider as bounds
        var collider = newEnemy.GetComponent<CapsuleCollider>();
        return collider.bounds.size;
    }

    void UpdateMask(List<GameObject> allowed)
    {
        for(int i = 0, n = enemyManagersMask.Length; i < n; i++)
            enemyManagersMask[i] = false;


        // Boo O(N^2) but not called often & N very small
        for(int i = 0, n = enemyManagers.Length; i < n; i++)
        {
            for(int j = 0, k = allowed.Count; j < k; j++)
            {
                if (enemyManagers[i].EnemyPrefab == allowed[j])
                    enemyManagersMask[i] = true;
            }
        }
    }
}
