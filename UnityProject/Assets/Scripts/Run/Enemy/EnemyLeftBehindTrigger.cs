﻿using UnityEngine;

public class EnemyLeftBehindTrigger : MonoBehaviour
{
    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("Enemy left behind.");

        EventManager.Trigger(new RemoveEnemyRequest() { enemy = collider.gameObject });
    }
}
