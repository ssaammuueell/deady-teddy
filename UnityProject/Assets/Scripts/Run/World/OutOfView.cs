﻿using UnityEngine;
using System.Collections;

public class OutOfView : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Offset behind the player (positive value).")]
    float playerOffset = 0.0f;
    [SerializeField]
    [Tooltip("Player position.")]
    Transform playerPosition;

    void Awake()
    {
        if (playerPosition == null)
            throw new MissingReferenceException("OutOfView is missing a reference to the player transform.");
    }

    void Update()
    {
        transform.position = new Vector3(0.0f, 0.0f, playerPosition.position.z - playerOffset);
    }

}
