﻿using UnityEngine;
using System.Collections;

public class PickupLeftBehindTrigger : MonoBehaviour
{
    void OnTriggerExit(Collider collider)
    {
        Debug.Log("Pickup left behind.");

        EventManager.Trigger(new RemovePickupRequest() { pickup = collider.GetComponent<Pickup>()});
    }
}
