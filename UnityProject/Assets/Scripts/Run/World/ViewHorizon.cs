﻿using UnityEngine;
using System.Collections;

public struct PopulateChunkRequest
{
    public GameObject chunk;
    public Transform viewHorizon;
}

public class ViewHorizon : MonoBehaviour
{

    Collider lastCollider = null;
    void OnTriggerEnter(Collider collider)
    {
        // Some jitter could potentially cause the same collider to trigger
        // more than once, check just in case
        if (lastCollider == collider)
            return;
        lastCollider = collider;

        // Notify manager
        EventManager.Trigger(new PopulateChunkRequest()
            {
                // Chunk is the parent of the floor
                chunk = collider.transform.parent.gameObject,
                viewHorizon = this.transform
            });
    }
}
