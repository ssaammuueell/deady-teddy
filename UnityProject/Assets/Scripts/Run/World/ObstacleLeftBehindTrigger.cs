﻿using UnityEngine;
using System.Collections;

public class ObstacleLeftBehindTrigger : MonoBehaviour
{
    void OnTriggerExit(Collider collider)
    {
        Debug.Log("Obstacle left behind.");

        EventManager.Trigger(new RemoveObstacleRequest() { obstacle = collider.gameObject });
    }
}
