﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct AddObstacleRequest
{
    public GameObject obstaclePrefab;
    public GameObject worldChunk;
    public float zNormalisedPosition; // Normalised [0,1] [start,end] z distance along chunk
    public float xNormalisedPosition; // Normalised [0,1] [left,right] distance from wall
}

public struct RemoveObstacleRequest
{
    public GameObject obstacle;
}

public class ObstacleManager : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Collider for left wall.")]
    Collider leftWall;
    [SerializeField]
    [Tooltip("Collider for right wall.")]
    Collider rightWall;

    Dictionary<GameObject, Queue<GameObject>> recycledObstacles = new Dictionary<GameObject,Queue<GameObject>>();
    Dictionary<GameObject, GameObject> instanceToPrefab = new Dictionary<GameObject,GameObject>();

    float distanceBetweenWalls;
    float wallLength;

    void Awake()
    {
        EventManager.AddListener<AddObstacleRequest>(OnAddObstacleRequest);
        EventManager.AddListener<RemoveObstacleRequest>(OnRemoveObstacleRequest);

        distanceBetweenWalls = rightWall.collider.bounds.min.x - leftWall.collider.bounds.max.x;

        // NOTE SP - in local axis, the model is messed up so x => z and viceversa
        wallLength = rightWall.GetComponent<BoxCollider>().size.x;
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<RemoveObstacleRequest>(OnRemoveObstacleRequest);
        EventManager.RemoveListener<AddObstacleRequest>(OnAddObstacleRequest);
    }

    void OnAddObstacleRequest(AddObstacleRequest e)
    {
        var bounds = e.obstaclePrefab.collider.bounds;

        // Calculate x position
        // note x=0 is the center
        float xrange = distanceBetweenWalls - bounds.size.x;
        float xmin = bounds.center.x - (0.5f * distanceBetweenWalls);
        float xPosition = xmin + (xrange * e.xNormalisedPosition);

        // Calculate y position
        // note z = 0 is the center
        float zrange = wallLength - bounds.size.z;
        float zmin = bounds.center.z - (0.5f * wallLength) + e.worldChunk.transform.position.z;
        float zPosition = zmin + (zrange * e.zNormalisedPosition);

        var pos = new Vector3(xPosition, 0.0f, zPosition);
        var obstacle = GetObstacleInstance(e.obstaclePrefab, pos);
        obstacle.transform.SetParent(this.transform);

    }

    void OnRemoveObstacleRequest(RemoveObstacleRequest e)
    {
        // Only obstacles created by the obstacle manager should be
        // deleted by it. If not found warn & destroy instead of recycle.
        if (!instanceToPrefab.ContainsKey(e.obstacle))
        {
            Debug.LogWarning(string.Format("Obstacle \"{0}\" not found in obstacle manager cache.", e.obstacle.name));
            GameObject.Destroy(e.obstacle);
            return;
        }

        // Recycle obstacle
        GameObject prefabSource = instanceToPrefab[e.obstacle];
        instanceToPrefab.Remove(e.obstacle);
        if (!recycledObstacles.ContainsKey(prefabSource))
        {
            recycledObstacles.Add(prefabSource, new Queue<GameObject>());
        }
        recycledObstacles[prefabSource].Enqueue(e.obstacle);
        e.obstacle.SetActive(false);
    }

    GameObject GetObstacleInstance(GameObject prefab, Vector3 pos)
    {
        // Try and reuse obstacles
        GameObject obstacle = null;
        if(recycledObstacles.ContainsKey(prefab))
        {
            var queue = recycledObstacles[prefab];
            if (queue.Count > 0)
            {
                obstacle = queue.Dequeue();
                obstacle.SetActive(true);
            }
        }

        // If non available, instantiate new
        if (obstacle == null)
        {
            obstacle = (GameObject)GameObject.Instantiate(prefab);
            obstacle.AddComponent<ObstacleColliderInfo>();
        }      
        instanceToPrefab.Add(obstacle, prefab);

        obstacle.transform.position = pos;  

        // Precalculate collider info and return
        var info = obstacle.GetComponent<ObstacleColliderInfo>();
        info.Calculate(leftWall, rightWall);
        return obstacle;
    }
}
