﻿using UnityEngine;
using System.Collections;

public struct NextWorldChunkRequest { };

public class NextWorldChunkTrigger : MonoBehaviour
{
	void OnTriggerEnter(Collider collider)
    {
        EventManager.Trigger(new NextWorldChunkRequest());
    }
}
