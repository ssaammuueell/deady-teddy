﻿using UnityEngine;

public struct PickupEvent
{
    public Pickup pickup;
}

public class Pickup : MonoBehaviour
{
    [SerializeField]
    [Tooltip("ID of the weapon. Empty for just ammo, no weapon change.")]
    string weaponID = string.Empty;
    [SerializeField]
    [Tooltip("How likely this pickup is to appear. This number will be normalised with the total of all pickups.")]
    float likelihood = 0.0f;
    [SerializeField]
    [Tooltip("Sound effect to play on pickup")]
    AudioClip sound = null;

    public string WeaponID { get { return weaponID; } }
    public float Likelihood { get { return likelihood; } }
    public AudioClip Sound { get { return sound; } }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.layer != CollisionLayer.Player.ToInt())
            return;

        EventManager.Trigger(new PickupEvent() { pickup = this });
    }
}
