﻿using UnityEngine;
using System.Collections;

public class ObstacleColliderInfo : MonoBehaviour
{
    float leftWallGap;
    float rightWallGap;
    Bounds bounds;

    public float LeftWallGap { get { return leftWallGap; } }
    public float RightWallGap { get { return rightWallGap; } }
    public Bounds Bounds { get { return bounds; } }

    void Start()
    {
        CalculateBounds();
    }

    void CalculateBounds()
    {
        /*var colliders = GetComponents<Collider>();
        if (colliders.Length == 0)
        {
            bounds = new Bounds();
            return;
        }

        // Create bounds containing all colliders
        bounds = colliders[0].bounds;
        foreach(var c in colliders)
        {
            bounds.Encapsulate(c.bounds);
        }*/

        bounds = GetComponent<Collider>().bounds;
    }

#if UNITY_EDITOR // In editor mode, update dynamically so we can move obstacles around for testing
    Collider leftWall, rightWall;
    public void Calculate(Collider leftWall, Collider rightWall)
    {
        this.leftWall = leftWall;
        this.rightWall = rightWall;
    }
    void Update()
    {
        CalculateBounds();
        leftWallGap = bounds.min.x - leftWall.bounds.max.x;
        rightWallGap = rightWall.bounds.min.x - bounds.max.x;
    }
#else
    public void Calculate(Collider leftWall, Collider rightWall)
    {
        CalculateBounds();
        leftWallGap = bounds.min.x - leftWall.bounds.max.x;
        rightWallGap = rightWall.bounds.min.x - bounds.max.x;
    }
#endif
}
