﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class WorldRegion : MonoBehaviour
{
    [SerializeField]
    [Range(1,10)]
    [Tooltip("Number of world chunks this region lasts.")]
    int chunkCount = 1;
    [SerializeField]
    [Tooltip("Enemies allowed in this region.")]
    List<GameObject> enemyPrefabs = new List<GameObject>();
    [SerializeField]
    [Tooltip("Enemy intensity ratio multiplier.")]
    float enemyIntensity = 1.0f;


    public int ChunkCount { get { return chunkCount; } }
    public List<GameObject> EnemiesAllowed { get { return enemyPrefabs; } }
    public float EnemyIntensity { get { return enemyIntensity; } }

    public abstract void NextWorldChunk(GameObject chunk);
}


class WorldPatternIterator : IEnumerator<WorldRegion>
{
    WorldRegion[] regions;
    int regionIndex;
    int chunksUntilNextRegion;
    int patternIteration;

    public int PatternIteration { get { return patternIteration; } }

    public WorldPatternIterator(WorldRegion[] regions)
    {
        this.regions = (WorldRegion[])regions.Clone();
        Reset();
    }

    public WorldRegion Current
    {
        get { return regions.Length > 0 ? regions[regionIndex] : null; }
    }

    object IEnumerator.Current
    {
        get { return regions.Length > 0 ? regions[regionIndex] : null; }
    }

    public bool MoveNext()
    {
        if (regions.Length == 0)
            return false;

        // Not done with this region
        chunksUntilNextRegion--;
        if (chunksUntilNextRegion > 0)
            return false;

        // Move onto next region & check for end of pattern
        regionIndex++;
        if (regionIndex >= regions.Length)
        {
            regionIndex = 0;
            patternIteration++;
        }
        chunksUntilNextRegion = regions[regionIndex].ChunkCount;
        return true;
    }

    public void Reset()
    {
        chunksUntilNextRegion = this.regions.Length > 0 ? this.regions[0].ChunkCount : -1;
        regionIndex = 0;
        patternIteration = 1;
    }

    public void Dispose()
    {
    }
}
