﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleWorldRegion : WorldRegion
{
    [SerializeField]
    [Tooltip("Obstacles allowed.")]
    List<GameObject> obstaclePrefabs = new List<GameObject>();
    [SerializeField]
    [Tooltip("Number of obstacles per chunk")]
    int obstaclesPerChunk = 0;


    int chunkIndex = 0;

    public override void NextWorldChunk(GameObject chunk)
    {
        if (obstaclePrefabs.Count > 0)
        {
            // Equally spaced obstacles along z-axis with random x-axis value
            for (int i = 0; i < obstaclesPerChunk; i++)
            {
                // Space them equally along the multiple chunks
                int j = i + (chunkIndex * obstaclesPerChunk);
                float xRegionNorm = (float)(j + 1) / (float)(ChunkCount * (obstaclesPerChunk + 1));
                float xChunkNorm = (xRegionNorm * (float)ChunkCount) % 1.0f;

                EventManager.Trigger(new AddObstacleRequest()
                {
                    obstaclePrefab = obstaclePrefabs[Random.Range(0, obstaclePrefabs.Count)],
                    worldChunk = chunk,
                    zNormalisedPosition = xChunkNorm,
                    xNormalisedPosition = Random.Range(0.25f, 0.75f)
                });
            }               
        }

        // Update index
        chunkIndex = (chunkIndex + 1) % obstaclesPerChunk; 
    }
}
