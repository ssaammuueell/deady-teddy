﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct RemovePickupRequest
{
    public Pickup pickup;
}

public class PickupManager : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Minimum gap between pickups.")]
    float minPickupGap = 0.0f;
    [SerializeField]
    [Tooltip("Maximumgap between pickups.")]
    float maxPickupGap = 0.0f;
    [SerializeField]
    [Tooltip("Pickup prefabs.")]
    List<Pickup> pickups = new List<Pickup>();

    Dictionary<Pickup, Queue<Pickup>> recycled = new Dictionary<Pickup, Queue<Pickup>>();
    Dictionary<Pickup, Pickup> instanceToPrefab = new Dictionary<Pickup, Pickup>();

    float nextPickupDist;

    void Awake()
    {
        EventManager.AddListener<RemovePickupRequest>(OnPickupLeftBehind);
        EventManager.AddListener<PickupEvent>(OnPickupEvent);
        EventManager.AddListener<WorldChunkInViewEvent>(OnChunkInView);
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<RemovePickupRequest>(OnPickupLeftBehind);
        EventManager.RemoveListener<PickupEvent>(OnPickupEvent);
        EventManager.RemoveListener<WorldChunkInViewEvent>(OnChunkInView);
    }

    void Start()
    {
        nextPickupDist = Random.Range(minPickupGap, maxPickupGap);
    }

    void OnPickupLeftBehind(RemovePickupRequest e)
    {
        RecyclePickup(e.pickup);
    }

    void OnPickupEvent(PickupEvent e)
    {
        RecyclePickup(e.pickup);
    }

    void RecyclePickup(Pickup pickup)
    {
        // Should have been created my the manager
        if (!instanceToPrefab.ContainsKey(pickup))
        {
            Debug.LogWarning(string.Format("Pickup \"{0}\" not found in pickup manager cache.", pickup.name));
            GameObject.Destroy(pickup);
            return;
        }

        // Recycle obstacle
        var prefabSource = instanceToPrefab[pickup];
        instanceToPrefab.Remove(pickup);
        if (!recycled.ContainsKey(prefabSource))
        {
            recycled.Add(prefabSource, new Queue<Pickup>());
        }
        recycled[prefabSource].Enqueue(pickup);
        pickup.gameObject.SetActive(false);
    }

    void OnChunkInView(WorldChunkInViewEvent e)
    {
        // generate pickups within this chunk
        float d = e.chunkLength;
        float z = e.viewHorizon.position.z;
        while (d >= nextPickupDist)
        {
            d -= nextPickupDist;
            z += nextPickupDist;
            nextPickupDist = Random.Range(minPickupGap, maxPickupGap);

            // Calculate x
            float minGap = 1.0f;
            float xmin =  minGap - (0.5f * e.distanceBetweenWalls);
            float xrange = e.distanceBetweenWalls - 2.0f * minGap;
            float x = xmin + Random.Range(0.0f, 1.0f) * xrange;

            NextPickup(z, x);
        }

        // remainder
        nextPickupDist -= d;
    }

    void NextPickup(float z, float x)
    {
        int n = pickups.Count;
        if (n <= 0)
            return;

        // Calculate total likelihood
        float totalLikelihood = 0.0f;
        for (int i = 0; i < n; i++)
            totalLikelihood += pickups[i].Likelihood;

        // Get random pickup using likelihoods
        float r = Random.Range(0.0f, totalLikelihood);
        for (int i = 0; i < n; i++)
        {
            r -= pickups[i].Likelihood;
            if (r < float.Epsilon)
            {
                DeployPickup(pickups[i], z, x);
                return;
            }
        }
    }

    void DeployPickup(Pickup prefab, float z, float x)
    {
        // Try and reuse pickups
        Pickup instance = null;
        if (recycled.ContainsKey(prefab))
        {
            var queue = recycled[prefab];
            if (queue.Count > 0)
            {
                instance = queue.Dequeue();
                instance.gameObject.SetActive(true);
            }
        }

        // If non available, instantiate new
        if (instance == null)
        {
            instance = GameObject.Instantiate(prefab) as Pickup;
            instance.transform.SetParent(this.transform, true);
        }
        instanceToPrefab.Add(instance, prefab);

        // Position it
        instance.transform.position = prefab.transform.position;
        instance.transform.position += new Vector3(x, 0.0f, z);
    }
}
