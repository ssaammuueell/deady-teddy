﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public struct WorldChunkInViewEvent
{
    public GameObject chunk;
    public Transform playerTransform;
    public float chunkLength;
    public float distanceBetweenWalls;
    public WorldRegion worldRegion;
    public Transform viewHorizon;
}

public struct WorldChunkActiveEvent
{
    public GameObject chunk;
    public Transform playerTransform;
    public float chunkLength;
    public float distanceBetweenWalls;
}

public class WorldManager : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Distance between world chunks.")]
    float chunkSeparation = 0.0f;
    [SerializeField]
    [Tooltip("Width of a chunk.")]
    float chunkWidth = 0.0f;
    [SerializeField]
    [Tooltip("Base world chunk prefab.")]
    GameObject chunkPrefab = null;
    [SerializeField]
    [Range(2, 8)]
    [Tooltip("Maximum number of active chunks.")]
    int maxChunks = 3;
    [SerializeField]
    [Tooltip("Player transform.")]
    Transform playerTransform = null;

    float nextChunkOffset = 0.0f;
    Queue<GameObject> chunks = new Queue<GameObject>();

    WorldPatternIterator patternIterator;

    void Awake()
    {
        if (chunkPrefab == null)
            throw new MissingReferenceException("WorldManager world chunk prefab not set.");
        if (playerTransform == null)
            throw new MissingReferenceException("WorldManager player transform not set.");

        EventManager.AddListener<NextWorldChunkRequest>(OnWorldChunkRequest);
        EventManager.AddListener<PopulateChunkRequest>(OnPopulateChunkRequest);

        var regions = GetComponents<WorldRegion>();
        patternIterator = new WorldPatternIterator(regions);
    }

    void Start()
    {
        // Remove editor dummy chunk
        if (this.transform.childCount > 0)
            GameObject.Destroy(this.transform.GetChild(0).gameObject);

        GenerateInitialChunks();
    }

    void GenerateInitialChunks()
    {
        // Create chunks
        while(chunks.Count < maxChunks)
        {
           NextWorldChunk();
        }

        Debug.Log("Initial world chunks generated.");
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<NextWorldChunkRequest>(OnWorldChunkRequest);
        EventManager.RemoveListener<PopulateChunkRequest>(OnPopulateChunkRequest);
    }

    void OnWorldChunkRequest(NextWorldChunkRequest request)
    {
        NextWorldChunk();
        Debug.Log("Chunk completed.");

        // Notify new active chunk
        EventManager.Trigger(new WorldChunkActiveEvent()
            {
                chunk = this.chunks.Peek(),
                playerTransform = this.playerTransform,
                distanceBetweenWalls = this.chunkWidth,
                chunkLength = this.chunkSeparation
            });
    }

    void OnPopulateChunkRequest(PopulateChunkRequest e)
    {
        // Build content for this
        WorldRegion region = patternIterator.Current;
        if (region != null)
            region.NextWorldChunk(e.chunk);
        patternIterator.MoveNext();

        // Notify
        EventManager.Trigger(new WorldChunkInViewEvent()
            {
                chunk = this.chunks.Peek(),
                playerTransform = this.playerTransform,
                distanceBetweenWalls = this.chunkWidth,
                chunkLength = this.chunkSeparation,
                worldRegion = region,
                viewHorizon = e.viewHorizon
            });

    }

    void NextWorldChunk()
    {
        // Create a new chunk or reuse an old one
        GameObject newChunk = null;
        if (chunks.Count < maxChunks)
        {
            newChunk = (GameObject)GameObject.Instantiate(chunkPrefab);
            newChunk.transform.SetParent(this.transform);
        }
        else
        {
            // Update chunk at the bottom of the queue which is now out of sight
            // and push it to the back of the queue. Reusing chunks avoids creating &
            // destroying them continuously.
            newChunk = chunks.Dequeue();

        }
        chunks.Enqueue(newChunk);

        // Set the position of the chunk
        newChunk.transform.position = new Vector3(0.0f, 0.0f, nextChunkOffset);
        nextChunkOffset += chunkSeparation;
    }
}
