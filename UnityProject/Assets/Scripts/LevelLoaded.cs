﻿using UnityEngine;
using System.Collections;

public struct LevelLoadedEvent
{
    public int level;
}

public class LevelLoaded : MonoBehaviour
{
    void OnLevelWasLoaded(int level)
    {
        EventManager.Trigger(new LevelLoadedEvent() { level = level });
    }
}
