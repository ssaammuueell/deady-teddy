﻿using UnityEngine;
using System.Collections;

public struct GameOverFadeCompleted
{

}

[RequireComponent(typeof(UnityEngine.UI.Image))]
public class GameOverFade : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Duration of the fade.")]
    float duration = 0.0f;

    void Start()
    {
        StartCoroutine(Fade(GetComponent<UnityEngine.UI.Image>(), duration));
    }

    static IEnumerator Fade(UnityEngine.UI.Image img, float duration)
    {
        float endTime = Time.time + duration;
        while (true)
        {
            if (Time.time >= endTime)
            {
                img.color = new Color(img.color.r, img.color.g, img.color.b, 1.0f);
                EventManager.Trigger(new GameOverFadeCompleted());
                break;
            }

            img.color = new Color(img.color.r, img.color.g, img.color.b, 1.0f - ((endTime - Time.time) / duration));
            yield return null;
        }
    }
}
