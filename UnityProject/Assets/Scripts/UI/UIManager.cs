﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class UIManager : MonoBehaviour
{

    [SerializeField]
    [Tooltip("Available UI screen prefabs")]
    List<GameObject> uiScreens = new List<GameObject>();

    struct Popup
    {
        public GameObject screen;
        public Action<object> onClosed;
    }

    static UIManager instance = null;
    static Dictionary<string, Popup> popups = new Dictionary<string, Popup>();
    static GameObject currentScreen = null;

    void Awake()
    {
        // Set instance;
        if (instance != null)
        {
            throw new Exception("UIManager singleton instance already exists.");
        }
        instance = this;
    }

    public static void SetScreen(string name)
    {
        // Clear popups to give them a chance to trigger their callbacks.
        CloseAllPopups();
        if (currentScreen != null)
            GameObject.Destroy(currentScreen);

        // Load the empty scene to clear the scene
        // and then load the new screen on top
        Application.LoadLevel("Empty");
        currentScreen = OpenScreen(name);
    }

    public static GameObject OpenPopup(string name, Action<object> onClosed)
    {
        var screen = OpenScreen(name);
        if (screen != null)
        {
            popups.Add(name, new Popup{screen = screen, onClosed = onClosed});
        }
        return screen;
    }

    static GameObject OpenScreen(string name)
    {
        // Find by name
        GameObject prefab = null;
        foreach(var s in instance.uiScreens)
        {
            if (s.name == name)
            {
                prefab = s;
                break;
            }
        }

        // Not found?
        if (prefab == null)
        {
            throw new System.ArgumentException(string.Format("UIManager could not find screen prefab: {0}.", name));
        }

        // Launch screen
        var screen = GameObject.Instantiate(prefab) as GameObject;
        screen.transform.SetParent(instance.transform, false);
        return screen;
    }

    public static void CloseAllPopups()
    {
        if (popups.Count == 0)
            return;

        // Cache and clear popups before closing them
        // to avoid problems with callbacks closing
        // popups themselves indirectly by setting a new screen
        var _popups = popups.Values.ToList();
        popups.Clear();

        // Close popups
        foreach(var popup in _popups)
            ClosePopupImpl(popup, null);
    }

    public static void ClosePopup(string name, object result)
    {
        if (!popups.ContainsKey(name))
        {
            Debug.LogWarning(string.Format("No popup {0} open.", name));
        }

        // Remove
        var popup = popups[name];
        popups.Remove(name);
        ClosePopupImpl(popup, result);
    }

    static void ClosePopupImpl(Popup popup, object result)
    {
        GameObject.Destroy(popup.screen);

        // Trigger callback
        if (popup.onClosed != null)
            popup.onClosed(result);
    }
}
