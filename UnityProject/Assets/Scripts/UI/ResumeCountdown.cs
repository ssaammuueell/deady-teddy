﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UnityEngine.UI.Text))]
public class ResumeCountdown : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Number of to count down from.")]
    private int count = 3;
    [SerializeField]
    [Tooltip("Time between counts.")]
    private float time = 1.0f;


    UnityEngine.UI.Text label;
    private int current;
    private float timeTilNext;

	void Start()
    {
        label = GetComponent<UnityEngine.UI.Text>();
        current = count;
        timeTilNext = time;
        label.text = current.ToString();  
	}

    void Update()
    {
        // Use unscaled time as this is executed when
        // the game is paused
        timeTilNext -= Time.unscaledDeltaTime;
        if (timeTilNext < 0.0f)
        {
            Countdown();
            timeTilNext += time;
        }
    }

    void Countdown()    
    { 
        current--;
        if (current <= 0)
        {
            UIManager.ClosePopup("ResumeCountdown", null);
        }

        label.text = current.ToString(); 
    }
}
