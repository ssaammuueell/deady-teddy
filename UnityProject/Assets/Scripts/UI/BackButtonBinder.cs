﻿using UnityEngine;

public class BackButtonBinder : DelegateBinderBase
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            TriggerDelegate();
    }
}