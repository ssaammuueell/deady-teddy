﻿using UnityEngine;
using System.Collections;
using System;

public abstract class DelegateBinderBase : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Delegate name.")]
    string callbackName = string.Empty;

    public event Action<string> OnDelegateTriggered;

    protected void TriggerDelegate()
    {
        var handler = OnDelegateTriggered;
        if (handler != null)
            handler(callbackName);
    }
}

[RequireComponent(typeof(UnityEngine.UI.Button))]
public class DelegateBinder : DelegateBinderBase
{
    [SerializeField]
    [Tooltip("Check this property to see if the button can be clicked. Empty for no condition.")]
    string canExecute = string.Empty;
    [SerializeField]
    [Tooltip("Invert the condition above.")]
    bool invertCanExecute = false;

    public string CanExecutePropertyName { get { return canExecute; } }
    public bool InvertCanExecute { get { return invertCanExecute; } }

    void Start()
    {
        var button = GetComponent<UnityEngine.UI.Button>();
        button.onClick.AddListener(() => TriggerDelegate());
    }

}

[ViewModelListenerAttribute(typeof(DelegateBinder))]
public class EnableDelegateViewModelListener : IViewModelListener
{
    DelegateBinder binder = null;

    public EnableDelegateViewModelListener(IViewModelListenerService service, DelegateBinder binder)
    {
        this.binder = binder;

        if (!string.IsNullOrEmpty(binder.CanExecutePropertyName))
        {
            // Get property & viemodel
            var viewModel = service.ViewModelInstance;
            var viewModelProperty = viewModel.GetType().GetProperty(binder.CanExecutePropertyName);

            if (viewModelProperty != null && viewModelProperty.PropertyType == typeof(bool))
            {
                service.SubscribeToProperty(binder.CanExecutePropertyName, this);
            }
        }
    }

    public void OnPropertyChanged(string name, object value)
    {
        // Get and adjust value
        var button = binder.GetComponent<UnityEngine.UI.Button>();
        bool newValue = (bool)value;
        if (binder.InvertCanExecute)
            newValue = !newValue;

        // Update UI
        if (button.interactable != newValue)
            button.interactable = newValue;
    }
}