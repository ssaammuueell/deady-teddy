﻿using System;
using System.Collections.Generic;
using System.Linq;

// Classes that implement this interface must have a constructor
// of type ctor(IViewModelServiceProvider, Component)
public interface IViewModelListener
{
    void OnPropertyChanged(string name, object value);
}

public class ViewModelListenerAttribute : Attribute
{
    private Type[] componentTypes;
    private int priority;

    // Components interested in
    public Type[] ComponentTypes { get { return componentTypes; } }
    // Initial binding update priority lower values will receive the initial
    // update first. This is important for listeners that depend on eachother.
    public int Priority { get { return priority; } }

    public ViewModelListenerAttribute(params Type[] types)
        : this(0, types)
    {
    }

    public ViewModelListenerAttribute(int priority, params Type[] types)
    {
        this.priority = priority;
        // Remove duplicates & get types that derive from UnityEngine.Component
        componentTypes = types
            .Where(t => !t.IsAbstract && t.IsSubclassOf(typeof(UnityEngine.Component)))
            .GroupBy(t => t)
            .Select(tt => tt.First())
            .ToArray();
    }
}

public class ViewModelListenerPriorityComparer : IComparer<IViewModelListener>
{
    public int Compare(IViewModelListener x, IViewModelListener y)
    {
        var xAttr = (ViewModelListenerAttribute)Attribute.GetCustomAttribute(x.GetType(), typeof(ViewModelListenerAttribute));
        var yAttr = (ViewModelListenerAttribute)Attribute.GetCustomAttribute(y.GetType(), typeof(ViewModelListenerAttribute));

        return xAttr.Priority - yAttr.Priority;
    }
}