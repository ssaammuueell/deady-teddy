﻿using System.Collections.Generic;
using System.Text;
using System.Linq;
using UnityEngine;
using System.Text.RegularExpressions;

[ExecuteInEditMode]
[RequireComponent(typeof(UnityEngine.UI.Text))]
public class LabelBinder : MonoBehaviour
{
    UnityEngine.UI.Text label;

    public string FormatedString { get; private set; }
    public string Text
    {
        get { return label.text; }
        set { label.text = value; }
    }

    void Awake()
    {
        label = GetComponent<UnityEngine.UI.Text>();

        // Store initial text directly from label
        FormatedString = label.text;
    }
}

[ViewModelListenerAttribute(typeof(LabelBinder))]
public class LabelViewModelListener : IViewModelListener
{
    Dictionary<string, string> parameterValues;
    Dictionary<string, string> parameterToVariable;
    LabelBinder binder;
    StringBuilder builder = new StringBuilder();

    public LabelViewModelListener(IViewModelListenerService service, LabelBinder binder)
    {
        this.binder = binder;

        // Parameters
        var parameters = ExtractParameters();
        parameterValues = parameters
            .ToDictionary(p => p, p => string.Empty);
        parameterToVariable = parameterValues
            .ToDictionary(p => p.Key, p => string.Concat("${", p.Key, "}"));

        // Subscribe
        foreach (var p in parameters)
            service.SubscribeToProperty(p, this);
    }

    public void OnPropertyChanged(string name, object value)
    {
        parameterValues[name] = (value != null ? value.ToString() : string.Empty);
        UpdateText();
    }

    private void UpdateText()
    {
        builder.Remove(0, builder.Length);
        builder.Append(binder.FormatedString);
        foreach (var kv in parameterValues)
        {
            builder.Replace(parameterToVariable[kv.Key], kv.Value);
        }
        binder.Text = builder.ToString();
    }
    List<string> ExtractParameters()
    {
        var vars = new List<string>();

        const string pattern = @"(\${[a-zA-Z_][a-zA-Z0-9_]*})"; // ${PropertyName}
        Regex rgx = new Regex(pattern);

        MatchCollection matches = rgx.Matches(binder.FormatedString);
        if (matches.Count > 0)
        {
            foreach (Match match in matches)
                vars.Add(match.Value.Substring(2, match.Value.Length - 3));
        }

        // No duplicates
        return vars
            .GroupBy(p => p)
            .Select(pp => pp.First())
            .ToList();
    }
}