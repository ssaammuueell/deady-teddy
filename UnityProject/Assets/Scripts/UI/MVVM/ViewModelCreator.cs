﻿using System;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class ViewModelPropertyAttribute : PropertyAttribute
{
}

public class ViewModelCreator : ViewModelProvider
{
    [SerializeField]
    [ViewModelPropertyAttribute]
    [Tooltip("Type of the view model.")]
    string viewModelType;

    protected override void Awake()
    {
        base.Awake();

        var vm = CreateInstance(viewModelType);
        SetViewModel(vm);
    }

    void OnDestroy()
    {
        // View model creator disposes the instance it created
        if(ViewModel != null)
        {
            ViewModel.Dispose();
        }
    }

    static ViewModel CreateInstance(string className)
    {
        if (string.IsNullOrEmpty(className))
            return null;

        var type = Assembly.GetExecutingAssembly().GetTypes()
            .First(t => t.Name == className);

        return Activator.CreateInstance(type) as ViewModel;
    }
}