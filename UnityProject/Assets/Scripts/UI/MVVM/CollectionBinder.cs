﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionBinder : MonoBehaviour
{
    [SerializeField]
    string listPropertyName = string.Empty;
    [SerializeField]
    string selectedPropertyName = string.Empty;
    [SerializeField]
    UnityEngine.UI.Button dynamicTemplate;
    [SerializeField]
    GameObject marker;

    IList dynamicList = null;
    UnityEngine.UI.Button selectedItem = null;
    List<UnityEngine.UI.Button> items = new List<UnityEngine.UI.Button>();



    public event Action<object> OnSelectionChanged;

    public string ListPropertyName { get { return listPropertyName; } }
    public string SelectedPropertyName { get { return selectedPropertyName; } }
    public bool IsDynamicList { get { return dynamicTemplate != null; } }

    int SelectedIndex { get { return selectedItem == null || items.Count == 0 ? -1 : items.IndexOf(selectedItem); } }


    void Start()
    {
        // Add child buttons and items for static lists
        if (!IsDynamicList)
        {
            foreach(Transform child in transform)
            {
                var btn = child.GetComponent<UnityEngine.UI.Button>();
                if (btn != null)
                {
                    items.Add(btn);
                    btn.onClick.AddListener(() => OnItemClicked(btn));
                }
            }
        }
    }

    public void SetDynamicList(IList list)
    {
        dynamicList = list;

        // Remove children
        foreach(Transform child in transform)
        {
            // Do not destroy marker
            if (marker == child.gameObject)
                continue;

            GameObject.Destroy(child.gameObject);
        }
        items.Clear(); 

        // Add new children
        int id = 0;
        foreach(var src in list)
        {
            // Create item
            var itemBtn = GameObject.Instantiate(dynamicTemplate) as UnityEngine.UI.Button;
            itemBtn.name += id++;
            itemBtn.transform.SetParent(this.transform, false);
            // Register to the button
            itemBtn.onClick.AddListener(() => OnItemClicked(itemBtn));

            // Give it its viewmodel is the list contains a view model type
            if (src.GetType().IsSubclassOf(typeof(ViewModel)))
            {
                var vmProvider = itemBtn.gameObject.AddComponent<ChildViewModelBinder>();
                vmProvider.SetViewModel(src as ViewModel);
            }

            items.Add(itemBtn);
        }

        SetSelection(null);
    }

    void OnItemClicked(UnityEngine.UI.Button btn)
    {
        if (btn != selectedItem)
        {
            selectedItem = btn;

            var handler = OnSelectionChanged;
            if (handler != null)
            {
                // For dynamic lists update view model with the selected object
                // For static lists, there are no objects associated, so instead
                // the selected property must be of type string and it will be set
                // to the name of the UI object
                int index = SelectedIndex;
                if (dynamicList != null)
                    handler(index >= 0 ? dynamicList[index] : null);
                else
                    handler(index >= 0 ? items[index].name : null);
            }
        }
    }

    public void SetSelection(object value)
    {
        // Dynamic: search list
        if (dynamicList != null)
        {
            int index = dynamicList.IndexOf(value);
            if (index != SelectedIndex)
                selectedItem = (index >= 0 ? items[index] : null);
        }
        // Static: search scene children
        else
        {
            selectedItem = null;
            string stringValue = value as string;
            if (stringValue == null)
                return;

            foreach (Transform child in transform)
            {
                // We added all children with button component, so use same
                // criteria for searching
                var btn = child.GetComponent<UnityEngine.UI.Button>();
                if (btn != null && btn.name == stringValue)
                {
                    selectedItem = btn;
                    break;
                }
            }
        }
    }

    void Update()
    {
        // Can't update the marker once when an item is selected
        // as when the initial value is set from the view model, if
        // the items are coined in a layout component, their positions
        // won't have been updated yet as they will have been created on
        // the same frame as SetSelection() was called.
        UpdateMarker();
    }

    void UpdateMarker()
    {
        if (marker != null)
        {
            // If nothing selected and marker active, deactivate it
            if (selectedItem == null)
            {
                if (marker.activeSelf)
                    marker.SetActive(false);
                return;
            }

            // Align marker & selection centers
            if (!marker.activeSelf)
                    marker.SetActive(true);
            marker.transform.position = selectedItem.transform.position;
        }
    }
}

[ViewModelListenerAttribute(typeof(CollectionBinder))]
public class ListSelectionViewModelListener : IViewModelListener
{
    bool firstUpdate = true;
    object currentValue = null;
    ViewModel viewModel;
    System.Reflection.PropertyInfo selectedProperty;
    CollectionBinder binder;

    public ListSelectionViewModelListener(IViewModelListenerService service, CollectionBinder binder)
    {
        this.binder = binder;

        if (!string.IsNullOrEmpty(binder.SelectedPropertyName))
        {
            // Get property & viemodel
            viewModel = service.ViewModelInstance;
            selectedProperty = viewModel.GetType().GetProperty(binder.SelectedPropertyName);

            if (selectedProperty != null)
            {
                // Two-way binding so subscribe to UI selection changed
                // and view model property changed
                service.SubscribeToProperty(binder.SelectedPropertyName, this);
                binder.OnSelectionChanged += OnValueChangedFromUI;
            }
        }
    }

    public void OnPropertyChanged(string name, object value)
    {
        if (firstUpdate || currentValue != value)
        {
            currentValue = value;
            binder.SetSelection(currentValue);
            firstUpdate = false;
        }
    }
    private void OnValueChangedFromUI(object value)
    {
        var newValue = value;
        if (firstUpdate || newValue != currentValue)
        {
            currentValue = newValue;
            selectedProperty.SetValue(viewModel, currentValue, null);
            firstUpdate = false;
        }
    }
}

// Note that this has priority > 0 to ensure that during the inital update
// the collection is set before the selected item.
[ViewModelListenerAttribute(1,typeof(CollectionBinder))]
public class ListViewModelListener : IViewModelListener
{
    ViewModel viewModel;
    System.Reflection.PropertyInfo listProperty;
    CollectionBinder binder;

    public ListViewModelListener(IViewModelListenerService service, CollectionBinder binder)
    {
        this.binder = binder;

        if (!string.IsNullOrEmpty(binder.ListPropertyName) && binder.IsDynamicList)
        {
            // Get property & viemodel
            viewModel = service.ViewModelInstance;
            listProperty = viewModel.GetType().GetProperty(binder.ListPropertyName);

            // Validity checks before subscribing
            if (listProperty != null &&
                typeof(IList).IsAssignableFrom(listProperty.PropertyType) &&
                listProperty.PropertyType.IsGenericType)
            {
                service.SubscribeToProperty(binder.ListPropertyName, this);
            }
        }
    }

    public void OnPropertyChanged(string name, object value)
    {
        binder.SetDynamicList((IList)value);
    }
}