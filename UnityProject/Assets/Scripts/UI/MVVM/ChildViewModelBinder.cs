﻿using UnityEngine;

public class ChildViewModelBinder : ViewModelProvider
{
    [SerializeField]
    [Tooltip("Name of the property.")]
    string propertyName = string.Empty;

    public string PropertyName { get { return propertyName; } }

    public new void SetViewModel(ViewModel vm)
    {
        // This is not accesible in base class as not ViewModelProvider
        // types allow changing view model
        base.SetViewModel(vm);
    }
}

[ViewModelListenerAttribute(typeof(ChildViewModelBinder))]
public class ChildViewModelListener : IViewModelListener
{
    ChildViewModelBinder binder = null;

    public ChildViewModelListener(IViewModelListenerService service, ChildViewModelBinder binder)
    {
        this.binder = binder;

        if (!string.IsNullOrEmpty(binder.PropertyName))
        {
            // Get property & viemodel
            var viewModel = service.ViewModelInstance;
            var viewModelProperty = viewModel.GetType().GetProperty(binder.PropertyName);

            if (viewModelProperty != null && viewModelProperty.PropertyType.IsSubclassOf(typeof(ViewModel)))
            {
                service.SubscribeToProperty(binder.PropertyName, this);
            }
        }
    }

    public void OnPropertyChanged(string name, object value)
    {
        if (binder.ViewModel != value)
        {
            binder.SetViewModel(value as ViewModel);
        }
    }
}