﻿using System;
public abstract class ViewModel : IDisposable
{
    public event Action<string> propertyChanged;
    public void OnPropertyChanged(string name)
    {
        var handler = propertyChanged;
        if (handler != null)
            handler(name);
    }

    public virtual void Dispose() { }
}