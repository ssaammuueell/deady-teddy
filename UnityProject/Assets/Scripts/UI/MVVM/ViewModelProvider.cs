﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

public interface IViewModelListenerService
{
    void SubscribeToProperty(string name, IViewModelListener listener);

    ViewModel ViewModelInstance { get; }
}

public class ViewModelListenerService : IViewModelListenerService
{
    Dictionary<string, List<IViewModelListener>> listeners = new Dictionary<string,List<IViewModelListener>>();
    ViewModel viewModelInstance;

    public ViewModel ViewModelInstance { get { return viewModelInstance; } }

    public ViewModelListenerService(ViewModel viewModelInstance)
    {
        this.viewModelInstance = viewModelInstance;
        viewModelInstance.propertyChanged += OnPropertyChanged;
    }

    public void SubscribeToProperty(string name, IViewModelListener listener)
    {
        if (!listeners.ContainsKey(name))
        {
            listeners.Add(name, new List<IViewModelListener>());
        }
        listeners[name].Add(listener);
    }

    void OnPropertyChanged(string name)
    {
        // Get new value
        object newValue = GetPropertyValue(name);

        // Notify listeners
        if (listeners.ContainsKey(name))
        {
            var propertyListeners = listeners[name];
            foreach (var listener in propertyListeners)
            {
                listener.OnPropertyChanged(name, newValue);
            }
        }
    }

    object GetPropertyValue(string name)
    {
        var property = viewModelInstance.GetType().GetProperty(name);
        if (property == null)
            return null;
        return property.GetValue(viewModelInstance, null);
    }

    public void InitialUpdate()
    {
        // Flatten listeners into single list
        var flatList = new List<KeyValuePair<string, IViewModelListener>>();
        foreach (var kv in listeners)
            flatList.AddRange(kv.Value.Select(x => new KeyValuePair<string, IViewModelListener>(kv.Key, x)));

        // Get key-value pairs in order of their listener priority
        var ordered = flatList
                      .OrderByDescending(x => x.Value, new ViewModelListenerPriorityComparer())
                      .ToList();

        // Update listeners with initial values
        foreach (var kv in ordered)
        {
            kv.Value.OnPropertyChanged(kv.Key, GetPropertyValue(kv.Key));
        }
    }
}


[DisallowMultipleComponent]
public abstract class ViewModelProvider : MonoBehaviour
{
    ViewModel viewModelInstance;
    ViewModelListenerService listenerService;

    public ViewModel ViewModel
    {
        get { return viewModelInstance; }
    }

    protected virtual void Awake()
    {
        // Subscribe to delegates
        SubscribeToDelegates();
    }

    protected void SetViewModel(ViewModel vm)
    {
        if (viewModelInstance == vm)
            return;

        // Clear up
        listenerService = null;

        // New values
        viewModelInstance = vm;

        // Initialise vm
        if (viewModelInstance != null)
        {
            // Init service
            listenerService = new ViewModelListenerService(viewModelInstance);
            
            // Add view model listeners
            var listenerTypes = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && typeof(IViewModelListener).IsAssignableFrom(t));
            foreach(var listenerType in listenerTypes)
            {
                var listenerAttr = (ViewModelListenerAttribute)Attribute.GetCustomAttribute(listenerType, typeof(ViewModelListenerAttribute));
                if (listenerAttr != null)
                {
                    // Get components of interests & initialise listener
                    var components = new List<Component>();
                    foreach(var componentType in listenerAttr.ComponentTypes)
                    {
                        components.AddRange(FindComponents(componentType));
                    }

                    // Create listeners
                    foreach(var component in components)
                    {
                        Activator.CreateInstance(listenerType, listenerService, component);
                    }
                }
            }

            listenerService.InitialUpdate();
        }
    }

    List<Component> FindComponents(Type type)
    {
        // Note that this object itself is not searched to
        // avoid infinite loops with child view model binders
        var components = new List<Component>();
        FindComponentsR(type, this.gameObject, components);
        return components;
    }

    static void FindComponentsR(Type type, GameObject root, List<Component> list)
    {
        foreach(Transform child in root.transform)
        {
            // Add components to list
            list.AddRange(child.GetComponents(type));

            // Recurse for those that dont contain a view model provider
            if (child.GetComponent<ViewModelProvider>() == null)
            {
                FindComponentsR(type, child.gameObject, list);
            }
        }
    }

    void SubscribeToDelegates()
    {
        var binders = FindComponents(typeof(DelegateBinderBase));
        foreach(DelegateBinderBase binder in binders)
        {
            binder.OnDelegateTriggered += OnDelegateTriggered;
        }
    }

    void OnDelegateTriggered(string name)
    {
        if (viewModelInstance == null || string.IsNullOrEmpty(name))
            return;

        var callback = viewModelInstance.GetType().GetMethod(name);
        if (callback == null)
            return;
        callback.Invoke(viewModelInstance, null);
    }
}