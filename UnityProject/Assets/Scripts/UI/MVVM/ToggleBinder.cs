﻿using System;
using UnityEngine;

[RequireComponent(typeof(UnityEngine.UI.Toggle))]
public class ToggleBinder : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Name of the boolean property.")]
    string propertyName = string.Empty;

    public string PropertyName { get { return propertyName; } }

    public event Action<bool> OnValueChanged;

    void Start()
    {
        var button = GetComponent<UnityEngine.UI.Toggle>();
        button.onValueChanged.AddListener((e) => TriggerValueChanged(e));
    }

    void TriggerValueChanged(bool value)
    {
        var handler = OnValueChanged;
        if (handler != null)
            handler(value);
    }
}

[ViewModelListenerAttribute(typeof(ToggleBinder))]
public class ToggleViewModelListener : IViewModelListener
{
    bool firstUpdate = true;
    bool currentValue;
    ViewModel viewModel;
    System.Reflection.PropertyInfo viewModelProperty;
    UnityEngine.UI.Toggle toggle;

    public ToggleViewModelListener(IViewModelListenerService service, ToggleBinder binder)
    {
        if (!string.IsNullOrEmpty(binder.PropertyName))
        {
            // Get property & viemodel
            viewModel = service.ViewModelInstance;
            viewModelProperty = viewModel.GetType().GetProperty(binder.PropertyName);
            toggle = binder.GetComponent<UnityEngine.UI.Toggle>();

            if (viewModelProperty != null && viewModelProperty.PropertyType == typeof(bool))
            {
                binder.OnValueChanged += OnValueChangedFromUI;
                service.SubscribeToProperty(binder.PropertyName, this);

                // Set initial value from UI
                currentValue = toggle.isOn;
            }
        }
    }
    public void OnPropertyChanged(string name, object value)
    {
        // This is a two-way binding system (UI updates view model and vice-versa)
        // so the callbacks need to check for value changed to avoid infinite loops
        bool newValue = (bool)value;
        if (firstUpdate || newValue != currentValue)
        {
            currentValue = newValue;
            toggle.isOn = currentValue;
            firstUpdate = false;
        }
    }

    private void OnValueChangedFromUI(bool value)
    {
        bool newValue = value;
        if (firstUpdate || newValue != currentValue)
        {
            currentValue = newValue;
            viewModelProperty.SetValue(viewModel, currentValue, null);
            firstUpdate = false;
        }
    }
}