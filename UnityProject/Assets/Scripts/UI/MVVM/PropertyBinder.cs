﻿using System;
using UnityEngine;

public class ObjectPropertyAttribure : PropertyAttribute
{

}

[Serializable]
public class ObjectProperty
{
    public UnityEngine.Object target;
    public string propertyName;
}

public class PropertyBinder : MonoBehaviour
{
    [SerializeField]
    [Tooltip("View model property source.")]
    string sourceProperty;
    [SerializeField]
    [ObjectPropertyAttribure]
    [Tooltip("Target property to set.")]
    ObjectProperty targetProperty;

    public string SourceProperty { get { return sourceProperty; } }
    public ObjectProperty TargetProperty { get { return targetProperty; } }
}

[ViewModelListenerAttribute(typeof(PropertyBinder))]
public class PropertyViewModelListener : IViewModelListener
{
    System.Reflection.PropertyInfo targetProperty;
    UnityEngine.Object target;

    public PropertyViewModelListener(IViewModelListenerService service, PropertyBinder binder)
    {
        if (!string.IsNullOrEmpty(binder.SourceProperty) &&
            (binder.TargetProperty != null) &&
            !string.IsNullOrEmpty(binder.TargetProperty.propertyName) &&
            (binder.TargetProperty.target != null))
        {
            // Get properties & viemodel
            var viewModel = service.ViewModelInstance;
            var viewModelProperty = viewModel.GetType().GetProperty(binder.SourceProperty);
            targetProperty = binder.TargetProperty.target.GetType().GetProperty(binder.TargetProperty.propertyName);
            target = binder.TargetProperty.target;

            // If valid and same type, subscribe
            if (viewModelProperty != null && 
                targetProperty != null &&
                viewModelProperty.PropertyType == targetProperty.PropertyType)
            {
                service.SubscribeToProperty(binder.SourceProperty, this);
            }
        }
    }

    public void OnPropertyChanged(string name, object value)
    {
        targetProperty.SetValue(target, value, null);
    }
}