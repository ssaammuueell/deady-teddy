﻿using UnityEngine;

[RequireComponent(typeof(UnityEngine.UI.Button))]
class ButtonSound : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Sound effect to play.")]
    AudioClip clip;

    void Awake()
    {
        // Play sound on click
        var button = GetComponent<UnityEngine.UI.Button>();
        button.onClick.AddListener(() => AudioManager.PlaySoundEffect(clip));
    }
}