﻿using System;
using System.Collections.Generic;

/// <summary>
/// Global event manager. Listeners register for events by type.
/// The even type is restricted to structs as we want to avoid
/// unnecesary heap allocations during gameplay.
/// </summary>
public static class EventManager
{
    private static Dictionary<Type, Delegate> listeners = new Dictionary<Type,Delegate>();

    public static void AddListener<T>(Action<T> callback) where T : struct
    {
        lock (listeners)
        {
            if (listeners.ContainsKey(typeof(T)))
            {
                listeners[typeof(T)] = (Action<T>)listeners[typeof(T)] + callback;
            }
            else
            {
                listeners[typeof(T)] = callback;
            }
        }
    }

    public static void RemoveListener<T>(Action<T> callback) where T : struct
    {
        lock (listeners)
        {
            if (listeners.ContainsKey(typeof(T)))
            {
                listeners[typeof(T)] = (Action<T>)listeners[typeof(T)] - callback;
            }
        }
    }

    public static void Trigger<T>(T evnt) where T : struct
    {
        if (listeners.ContainsKey(typeof(T)))
        {
            Action<T> handler = (Action<T>)listeners[typeof(T)];
            if (handler != null)
                handler(evnt);
        }
    }
}