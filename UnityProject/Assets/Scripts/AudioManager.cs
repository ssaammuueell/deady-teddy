﻿using UnityEngine;
using System.Collections;

public struct AudioSettingsChangedEvent
{
}

public class AudioManager : MonoBehaviour
{
    static AudioManager instance = null;
    AudioSource music = null;
    AudioSource sfx = null;

    public static float MusicVolume
    {
        get { return instance != null ? instance.music.volume : 0.0f; }
        set { if (instance != null) instance.music.volume = value; }
    }

    public static AudioClip BackgroundMusic
    {
        get { return instance != null ? instance.music.clip : null; }
        set
        {
            if (instance == null)
                return;

            instance.music.clip = value;
            if (value != null)
                instance.music.Play();
        }
    }

    public static void PlaySoundEffect(AudioClip clip)
    {
        if (instance == null)
            return;

        instance.sfx.PlayOneShot(clip);
    }

    void Awake()
    {
        if (instance != null)
        {
            throw new System.Exception("AudioManager singleton instance already exists.");
        }
        instance = this;

        EventManager.AddListener<AudioSettingsChangedEvent>(OnAudioSettingsChanged);
        Initialise();
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<AudioSettingsChangedEvent>(OnAudioSettingsChanged);
    }

    void Initialise()
    {
        // Add components programatically
        gameObject.AddComponent<AudioListener>();
        music = gameObject.AddComponent<AudioSource>();
        music.loop = true;
        sfx = gameObject.AddComponent<AudioSource>();

    }

    void Start()
    {
        // Initial settings
        UpdateSettings();
    }

    void UpdateSettings()
    {
        music.mute = (1 != PlayerPrefs.GetInt("MusicEnabled", 1));
        sfx.mute = (1 != PlayerPrefs.GetInt("SFXEnabled", 1));
    }

    void OnAudioSettingsChanged(AudioSettingsChangedEvent e)
    {
        UpdateSettings();
    }

}
