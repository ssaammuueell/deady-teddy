﻿using UnityEngine;
using System.Collections;

public class StartFromShell : MonoBehaviour
{
#if UNITY_EDITOR
    void Awake()
    {
        // Allow to run game directly from the Run scene in the editor
        // Purely for convenience when working on run scene. If the start-up
        // scene uses data instantiated in the shell scene in Awake() functions
        // errors may be thrown before Unity actually loads the Shell level.
        if (!GameRoot.Initialised)
        {
            Application.LoadLevel("Shell");
        }
    }
#endif
}
