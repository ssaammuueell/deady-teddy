﻿using UnityEngine;
using System.Collections;

public class GameRoot : MonoBehaviour
{
    static GameRoot instance = null;

    public static bool Initialised { get { return instance != null; } }

    void Awake()
    {
        // Set instance;
        if (instance != null)
        {
            throw new System.Exception("GameRoot singleton instance already exists.");
        }
        instance = this;

        // This root object persists across scene loads
        Object.DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        UIManager.SetScreen("MainScreen");
    }
}
