﻿using UnityEditor;
using UnityEngine;

public static class PlayerDataHelper
{
    [MenuItem("Deady Teddy/Clear Player Data")]
    static void Clear()
    {
        PlayerPrefs.DeleteAll();
    }

    [MenuItem("Deady Teddy/Give Coins")]
    static void GiveCoins()
    {
        PlayerPrefs.SetInt("Coins", 100000);
    }

    [MenuItem("Deady Teddy/Fill Ammo")]
    static void FillAmmo()
    {
        EventManager.Trigger(new FillAmmoEvent());
    }

    [MenuItem("Deady Teddy/Set Invincible/True")]
    static void SetInvincibleTrue()
    {
        EventManager.Trigger(new SetInvincibleEvent() { value = true });
    }

    [MenuItem("Deady Teddy/Set Invincible/False")]
    static void SetInvincibleFalse()
    {
        EventManager.Trigger(new SetInvincibleEvent() { value = false });
    }
}