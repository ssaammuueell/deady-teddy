﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Linq;
using System.Reflection;

[CustomPropertyDrawer(typeof(ObjectPropertyAttribure))]
public class ObjectPropertyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 2 * base.GetPropertyHeight(property, label); // 2 fields of default value
    }

    public override void OnGUI(
        Rect mainRect,
        SerializedProperty prop,
        GUIContent label)
    {
        // Divide area into 2 equal rects
        var rect1 = mainRect;
        rect1.height /= 2;
        var rect2 = rect1;
        rect2.y += rect2.height;

        // Get sub-properties
        var target = prop.FindPropertyRelative("target");
        var targetProperty = prop.FindPropertyRelative("propertyName");

        // Target object
        EditorGUI.BeginChangeCheck();
        var newTarget = EditorGUI.ObjectField(rect1, "Target Object", target.objectReferenceValue, typeof(UnityEngine.Object), true);
        if (EditorGUI.EndChangeCheck())
            target.objectReferenceValue = newTarget;

        // Find propery names
        string[] propNames = null;
        if (target.objectReferenceValue != null)
        {
            propNames = target.objectReferenceValue.GetType().GetProperties()
                .Select(p => p.Name)
                .ToArray();
        }
        else
        {
            propNames = new string[0];
        }

        // Target property
        EditorGUI.BeginChangeCheck();
        int oldIndex = Array.IndexOf(propNames, targetProperty.stringValue);
        int newIndex = EditorGUI.Popup(rect2, "Target object property", oldIndex, propNames);
        if (EditorGUI.EndChangeCheck())
            targetProperty.stringValue = (newIndex > 0 && newIndex < propNames.Length ? propNames[newIndex] : string.Empty);
    }
}