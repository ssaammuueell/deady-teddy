﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Linq;
using System.Reflection;

[CustomPropertyDrawer(typeof(ViewModelPropertyAttribute))]
public class ViewModelPropertyDrawer : PropertyDrawer
{
    string[] classNames;

    public ViewModelPropertyDrawer()
    {
        classNames = Assembly.GetAssembly(typeof(ViewModel)).GetTypes()
            .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(ViewModel)))
            .Select(x => x.Name)
            .ToArray();
    }

    public override void OnGUI(
        Rect position,
        SerializedProperty prop,
        GUIContent label)
    {
        EditorGUI.BeginChangeCheck();
        int oldIndex = Array.IndexOf(classNames, prop.stringValue);
        int newIndex = EditorGUI.Popup(position, "View Model", oldIndex, classNames);
        if (EditorGUI.EndChangeCheck())
            prop.stringValue = (newIndex > 0 && newIndex < classNames.Length ? classNames[newIndex] : string.Empty);
    }
}